<?php
class DC_Woodle_Settings_Gneral
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    private $tab;

    /**
     * Start up
     */
    public function __construct($tab)
    {
        $this->tab = $tab;
        $this->options = get_option("dc_{$this->tab}_settings_name");
        $this->settings_page_init();
    }

    /**
     * Register and add settings
     */

    public function settings_page_init()
    {
        global $DC_Woodle;

        $settings_tab_options = array(
            "tab" => "{$this->tab}",
            "ref" => & $this,
            "sections" => array(
                "moodle_settings_section" => array(
                    "title" => __('Moodle Settings', 'moodle-synh' ) , // Section one
                    "fields" => array(
                        "access_url" => array(
                            'title' => __('Access URL', 'moodle-synh' ) ,
                            'type' => 'url',
                            'id' => 'access_url',
                            'label_for' => 'access_url',
                            'name' => 'access_url',
                            'hints' => __('Enter the moodle site URL you want to integrate.', 'moodle-synh' ) ,
                            'desc' => __('Moodle site URL (Value of $CFG->wwwroot).', 'moodle-synh' )
                        ) , // Text
                        "ws_token" => array(
                            'title' => __('Webservice token', 'moodle-synh' ) ,
                            'type' => 'text',
                            'id' => 'ws_token',
                            'label_for' => 'ws_token',
                            'name' => 'ws_token',
                            'hints' => __('Enter the moodle webservice token.', 'moodle-synh' ) ,
                            'desc' => __('Moodle webservice token (Generate from moodle).', 'moodle-synh' )
                        ) , // Text
                        "ws_policy" => array(
                            'title' => __('Policy url', 'moodle-synh' ) ,
                            'type' => 'text',
                            'id' => 'ws_policy',
                            'label_for' => 'ws_policy',
                            'name' => 'ws_policy',
                            'hints' => __('Enter policy url.', 'moodle-synh' ) ,
                            'desc' => __('Moodle policy description', 'moodle-synh' )
                        ) , // Text
                        "ws_rtext_" . ICL_LANGUAGE_CODE => array(
                            'title' => __('Registration text', 'moodle-synh' ) ,
                            'type' => 'textarea',
                            'id' => 'ws_rtext_' . ICL_LANGUAGE_CODE,
                            'label_for' => 'ws_rtext_' . ICL_LANGUAGE_CODE,
                            'name' => 'ws_rtext_' . ICL_LANGUAGE_CODE,
                            'hints' => __('Enter registration instuction.', 'moodle-synh' ) ,
                            'desc' => __('Registration process description', 'moodle-synh' ) ,
                            'cols' => '100',
                            'rows' => 5
                        ) , // Text
                        "ws_countries_link" => array(
                            'title' => __('Moodle countries', 'moodle-synh' ) ,
                            'type' => 'text',
                            'id' => 'ws_countries_link',
                            'label_for' => 'ws_countries_link',
                            'name' => 'ws_countries_link',
                            'hints' => __('Enter link to moodle countries in json', 'moodle-synh' ) ,
                            'desc' => __('Enter link to moodle countries in json', 'moodle-synh' )
                        ) , // Text
                        "ws_specialties_link" => array(
                            'title' => __('Moodle specialties', 'moodle-synh' ) ,
                            'type' => 'text',
                            'id' => 'ws_specialties_link',
                            'label_for' => 'ws_specialties_link',
                            'name' => 'ws_specialties_link',
                            'hints' => __('Enter link to moodle specialties in json', 'moodle-synh' ) ,
                            'desc' => __('Enter link to moodle specialties in json', 'moodle-synh' )
                        ) , // Text
                        "ws_specialties_link_lt" => array(
                            'title' => __('Moodle specialties (LT)', 'moodle-synh' ) ,
                            'type' => 'text',
                            'id' => 'ws_specialties_link_lt',
                            'label_for' => 'ws_specialties_link_lt',
                            'name' => 'ws_specialties_link_lt',
                            'hints' => __('Enter link to moodle specialties in json', 'moodle-synh' ) ,
                            'desc' => __('Enter link to moodle specialties in json', 'moodle-synh' )
                        ) , // Text
                        "ws_logout_link" => array(
                            'title' => __('Logout url', 'moodle-synh' ) ,
                            'type' => 'text',
                            'id' => 'ws_logout_link',
                            'label_for' => 'ws_logout_link',
                            'name' => 'ws_logout_link',
                            'hints' => __('Enter custom logout url', 'moodle-synh' ) ,
                            'desc' => __('Enter custom logout url', 'moodle-synh' )
                        ) , // Text
                        "ws_moodle_login" => array(
                            'title' => __('Moodle login', 'moodle-synh' ) ,
                            'type' => 'text',
                            'id' => 'ws_moodle_login',
                            'label_for' => 'ws_moodle_login',
                            'name' => 'ws_moodle_login',
                            'hints' => __('Enter moodle API user login', 'moodle-synh' ) ,
                            'desc' => __('Enter moodle API user login', 'moodle-synh' )
                        ) , // Text
                        "ws_moodle_password" => array(
                            'title' => __('Moodle password', 'moodle-synh' ) ,
                            'type' => 'text',
                            'id' => 'ws_moodle_password',
                            'label_for' => 'ws_moodle_password',
                            'name' => 'ws_moodle_password',
                            'hints' => __('Enter moodle API user password', 'moodle-synh' ) ,
                            'desc' => __('Enter moodle API user password', 'moodle-synh' )
                        ) , // Text
                        "ws_moodle_email" => array(
                            'title' => __('E-mail', 'moodle-synh' ) ,
                            'type' => 'text',
                            'id' => 'ws_moodle_email',
                            'label_for' => 'ws_moodle_email',
                            'name' => 'ws_moodle_email',
                            'hints' => __('Enter e-mail ', 'moodle-synh' ) ,
                            'desc' => __('Enter moodle API user login', 'moodle-synh' )
                        ) , // Text
                        

                        "ws_moodle_email_name" => array(
                            'title' => __('Sender name', 'moodle-synh' ) ,
                            'type' => 'text',
                            'id' => 'ws_moodle_email_name',
                            'label_for' => 'ws_moodle_email_name',
                            'name' => 'ws_moodle_email_name',
                            'hints' => __('Enter Sender name ', 'moodle-synh' ) ,
                            'desc' => __('Enter Sender name', 'moodle-synh' )
                        ) , // Text
                        "ws_moodle_email_subject" => array(
                            'title' => __('E-mail subject (LV)', 'moodle-synh' ) ,
                            'type' => 'text',
                            'id' => 'ws_moodle_email_subject',
                            'label_for' => 'ws_moodle_email_subject',
                            'name' => 'ws_moodle_email_subject',
                            'hints' => __('Enter E-mail subject', 'moodle-synh' ) ,
                            'desc' => __('Enter E-mail subject', 'moodle-synh' )
                        ) , // Text
                        "ws_moodle_email_subject_lt" => array(
                            'title' => __('E-mail subject (LT)', 'moodle-synh' ) ,
                            'type' => 'text',
                            'id' => 'ws_moodle_email_subject_lt',
                            'label_for' => 'ws_moodle_email_subject_lt',
                            'name' => 'ws_moodle_email_subject_lt',
                            'hints' => __('Enter E-mail subject', 'moodle-synh' ) ,
                            'desc' => __('Enter E-mail subject', 'moodle-synh' )
                        ) , // Text
                        "ws_moodle_reg_txt" => array(
                            'title' => __('E-mail text (LT)', 'moodle-synh' ) ,
                            'type' => 'textarea',
                            'id' => 'ws_moodle_reg_txt',
                            'label_for' => 'ws_moodle_reg_txt',
                            'name' => 'ws_moodle_reg_txt',
                            'hints' => __('Enter e-mail text after registration ', 'moodle-synh' ) ,
                            'desc' => __('Enter e-mail text after registration', 'moodle-synh' ) ,
                            'cols' => '100',
                            'rows' => 5
                        ) , // Text
                        "ws_moodle_reg_txt_lt" => array(
                            'title' => __('E-mail text (LT)', 'moodle-synh' ) ,
                            'type' => 'textarea',
                            'id' => 'ws_moodle_reg_txt_lt',
                            'label_for' => 'ws_moodle_reg_txt_lt',
                            'name' => 'ws_moodle_reg_txt_lt',
                            'hints' => __('Enter e-mail text after registration ', 'moodle-synh' ) ,
                            'desc' => __('Enter e-mail text after registration', 'moodle-synh' ) ,
                            'cols' => '100',
                            'rows' => 5
                        ) , // Text
                        
                    )
                ) ,

                "wc_settings_section" => array(
                    "title" => "WooCommerce Product Settings", // Another section
                    "fields" => array(
                        "create_wc_product" => array(
                            'title' => __('Create products from courses', 'moodle-synh' ),
                            'type' => 'checkbox',
                            'id' => 'create_wc_product',
                            'label_for' => 'create_wc_product',
                            'name' => 'create_wc_product',
                            'hints' => __('Checked to create products from moodle courses.', 'moodle-synh' ),
                            'desc' => __('If checked products will created while syncing courses from moodle.', 'moodle-synh' ),
                            'value' => 'yes'
                        ),

                        /*"wc_product_dates_display" => array('title' => __('Display start date and end date in shop page', 'moodle-synh' ), 'type' => 'checkbox', 'id' => 'wc_product_dates_display', 'label_for' => 'wc_product_dates_display', 'name' => 'wc_product_dates_display', 'hints' => __('Checked to display dates in shop page under products.', 'moodle-synh' ), 'desc' => __('If checked display start date and end date in shop page.', 'moodle-synh' ),  'value' => 'yes')*/

                    )
                ) ,

                "wc_settings_section_la" => array(
                    "title" => "Latvijas Aptieka", // Another section
                    "fields" => array(

                        "enter_la_code" => array(
                            'title' => __('Latvijas Aptieka available by code', 'moodle-synh' ) ,
                            'type' => 'checkbox',
                            'id' => 'enter_la_code',
                            'label_for' => 'enter_la_code',
                            'name' => 'enter_la_code',
                            'hints' => __('Input code to access LA courses', 'moodle-synh' ) ,
                            'desc' => __('Input code to access LA courses', 'moodle-synh' ) ,
                            'value' => 'yes'
                        ) ,

                        "enter_la_code_txt" => array(
                            'title' => __('Kods ', 'moodle-synh' ) ,
                            'type' => 'text',
                            'id' => 'enter_la_code_txt',
                            'label_for' => 'enter_la_code_txt',
                            'name' => 'enter_la_code_txt',
                            'hints' => __('Enter Code', 'moodle-synh' ) ,
                            'desc' => __('Enter Code', 'moodle-synh' )
                        ) ,

                        "style_la_code" => array(
                            'title' => __('CSS for Latvijas Aptieka ', 'moodle-synh' ) ,
                            'type' => 'text',
                            'id' => 'v',
                            'label_for' => 'enter_la_code_txt',
                            'name' => 'style_la_code',
                            'hints' => __('Css style', 'moodle-synh' ) ,
                            'desc' => __('Css style for Latvijas Aptieka category', 'moodle-synh' )
                        ) ,

                    )
                ) ,

                /*"user_settings_section" => array("title" => "User Settings", // Another section
                "fields" => array(
                "update_existing_users" => array('title' => __('Update existing users', 'moodle-synh' ), 'type' => 'radio', 'id' => 'update_existing_users', 'label_for' => 'update_existing_users', 'name' => 'update_existing_users', 'options' => array('true' => 'Yes', 'false' => 'No'), 'dfvalue' => 'true',  'hints' => __('If enabled Moodle will update the profile fields.', 'moodle-synh' ), 'desc' => __('Whether Moodle will update the profile fields in Moodle for existing users.', 'moodle-synh' ))
                )
                
                ), */

            )
        );

        $DC_Woodle
            ->admin
            ->settings
            ->settings_field_init(apply_filters("settings_{$this->tab}_tab_options", $settings_tab_options));
    }
    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function dc_dc_woodle_general_settings_sanitize($input)
    {
        global $DC_Woodle;

        $new_input = array();

        $hasError = false;

        if (isset($input['access_url']))
        {
            $new_input['access_url'] = rtrim($input['access_url'], '/');
        }
        else
        {
            add_settings_error("dc_{$this->tab}_settings_name", esc_attr("dc_{$this->tab}_settings_admin_error") , __('Moodle access URL should not be empty.', 'moodle-synh' ) , 'error');
            $hasError = true;
        }

        if (isset($input['ws_policy']))
        {
            $new_input['ws_policy'] = sanitize_text_field($input['ws_policy']);
        }

        if (isset($input['ws_rtext_' . ICL_LANGUAGE_CODE]))
        {
            $new_input['ws_rtext_' . ICL_LANGUAGE_CODE] = $input['ws_rtext_' . ICL_LANGUAGE_CODE];
        }

        if (isset($input['ws_token']))
        {
            $new_input['ws_token'] = sanitize_text_field($input['ws_token']);
        }
        else
        {
            add_settings_error("dc_{$this->tab}_settings_name", esc_attr("dc_{$this->tab}_settings_admin_error") , __('Moodle webservice token should not be empty.', 'moodle-synh' ) , 'error');
            $hasError = true;
        }

        if (isset($input['ws_countries_link']))
        {
            $new_input['ws_countries_link'] = sanitize_text_field($input['ws_countries_link']);
        }
        if (isset($input['ws_specialties_link']))
        {
            $new_input['ws_specialties_link'] = sanitize_text_field($input['ws_specialties_link']);
        }
        if (isset($input['ws_specialties_link_lt']))
        {
            $new_input['ws_specialties_link_lt'] = sanitize_text_field($input['ws_specialties_link_lt']);
        }
        if (isset($input['ws_logout_link']))
        {
            $new_input['ws_logout_link'] = sanitize_text_field($input['ws_logout_link']);
        }

        if (isset($input['ws_moodle_login']))
        {
            $new_input['ws_moodle_login'] = sanitize_text_field($input['ws_moodle_login']);
        }
        if (isset($input['ws_moodle_password']))
        {
            $new_input['ws_moodle_password'] = sanitize_text_field($input['ws_moodle_password']);
        }

        if (isset($input['ws_moodle_email']))
        {
            $new_input['ws_moodle_email'] = sanitize_text_field($input['ws_moodle_email']);
        }
        if (isset($input['ws_moodle_email_name']))
        {
            $new_input['ws_moodle_email_name'] = sanitize_text_field($input['ws_moodle_email_name']);
        }
        if (isset($input['ws_moodle_reg_txt']))
        {
            $new_input['ws_moodle_reg_txt'] = ($input['ws_moodle_reg_txt']);
        }
        if (isset($input['ws_moodle_reg_txt_lt']))
        {
            $new_input['ws_moodle_reg_txt_lt'] = ($input['ws_moodle_reg_txt_lt']);
        }
        if (isset($input['ws_moodle_email_subject']))
        {
            $new_input['ws_moodle_email_subject'] = sanitize_text_field($input['ws_moodle_email_subject']);
        }
        if (isset($input['ws_moodle_email_subject_lt']))
        {
            $new_input['ws_moodle_email_subject_lt'] = sanitize_text_field($input['ws_moodle_email_subject_lt']);
        }

        if (isset($input['create_wc_product'])) $new_input['create_wc_product'] = $input['create_wc_product'];

        if (isset($input['enter_la_code'])) $new_input['enter_la_code'] = $input['enter_la_code'];

        if (isset($input['enter_la_code_txt'])) $new_input['enter_la_code_txt'] = $input['enter_la_code_txt'];

        if (isset($input['style_la_code'])) $new_input['style_la_code'] = $input['style_la_code'];

        if (isset($input['wc_product_dates_display'])) $new_input['wc_product_dates_display'] = $input['wc_product_dates_display'];

        if (isset($input['update_existing_users'])) $new_input['update_existing_users'] = $input['update_existing_users'];

        if (isset($input['update_user_info'])) $new_input['update_user_info'] = $input['update_user_info'];

        if (isset($input['moodle_role_id'])) $new_input['moodle_role_id'] = (!empty($input['moodle_role_id'])) ? intval($input['moodle_role_id']) : '';

        if (!$hasError)
        {
            add_settings_error("dc_{$this->tab}_settings_name", esc_attr("dc_{$this->tab}_settings_admin_updated") , __('General settings updated', 'moodle-synh' ) , 'updated');
        }

        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function moodle_settings_section_info()
    {
        global $DC_Woodle;

        _e('Enter your moodle settings below', 'moodle-synh' );
    }

    /** 
     * Print the Section text
     */
    public function wc_settings_section_info()
    {
        global $DC_Woodle;

        _e('Enter your WooCommerce settings below', 'moodle-synh' );
    }

    /** 
     * Print the Section text
     */
    public function user_settings_section_info()
    {
        global $DC_Woodle;

        _e('Enter user settings below', 'moodle-synh' );
    }
}

