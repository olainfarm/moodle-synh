<?php
/*
Plugin Name: moodle-synh
Description: Moodle synchronization plugin
Version:     1.3.0
Author:      Marina <marina@integra.lv>
Bitbucket Plugin URI: olainfarm/moodle-synh
*/
date_default_timezone_set( 'Europe/Riga' ); 
if ( ! class_exists( 'DC_Woodle_Dependencies' ) )
	require_once 'includes/class-dc-woodle-dependencies.php';

if( ! DC_Woodle_Dependencies::wc_active_check() )
  add_action( 'admin_notices', 'woodle_wc_inactive_notice' );

require_once 'includes/dc-woodle-core-functions.php';
require_once 'config.php';
require_once 'classes/class-curl.php';
require_once 'classes/list-countries.php';
require_once 'classes/list-pro.php';
$curl = new curl; 
global $curl;
  
if(!defined('ABSPATH')) exit; // Exit if accessed directly
if(!defined('DC_WOODLE_PLUGIN_TOKEN')) exit;
if(!defined('DC_WOODLE_TEXT_DOMAIN')) exit;
if(!defined('DC_WOODLE_PLUGIN_BASENAME')) 
	define('DC_WOODLE_PLUGIN_BASENAME', plugin_basename( __FILE__ ) );

require_once 'includes/class-dc-woodle-install.php';
register_activation_hook( __FILE__, array( 'DC_Woodle_Install', 'init' ) );

if( session_status() == PHP_SESSION_NONE ) {
	session_start();
}

if(!class_exists('DC_Woodle')) {
	require_once( 'classes/class-dc-woodle.php' );
	global $DC_Woodle;
	
	$DC_Woodle = new DC_Woodle( __FILE__ );
	$GLOBALS['DC_Woodle'] = $DC_Woodle;
}