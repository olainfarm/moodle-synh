<?php
class DC_Woodle_Actions {

	public function __construct() 
  {
    	 //add_action( 'moodle_logout', 'mw_moodle_logout');
       //do_action('mw_moodle_logout', $_GET['action']);
            if ($_GET['action']=='moodle_logout') {
                    $this->mw_moodle_logout();
            }
            
           if ($_POST['register']==1)
           {
                    $this->mw_moodle_rreg();
           }
       
	}
  
  
  	public function mw_moodle_logout( ) {
    
    global $DC_Woodle, $curl;
    //if (!empty($post->post_content)) { }
  
    $access_url = woodle_get_settings( 'access_url', 'dc_woodle_general' );
    $access_token = woodle_get_settings( 'ws_token', 'dc_woodle_general' ); 
    $ws_logout_link = woodle_get_settings( 'ws_logout_link', 'dc_woodle_general' );
	
	$full_url = $_SERVER['REQUEST_SCHEME'] .'://'. $_SERVER['HTTP_HOST'] . explode('?', $_SERVER['REQUEST_URI'], 2)[0];
    $serverurl = $ws_logout_link . '?return='.urlencode($full_url);
    header("Location: ".$serverurl); 
    //$resp1 = $curl->get($serverurl);
                
    //print_r($serverurl);            
    //echo 'aa'.$access_url;
        unset($_SESSION['moodle_user']);
        unset($_SESSION['moodle_redirect']);
    }
    

	
	
  	public function mw_moodle_rreg( ) {
    
    global $DC_Woodle, $curl, $moodle_reg_error, $moodle_d_array;
    
       $moodle_d_array = $d = $_POST;
       
       $d['username'] = trim ($d['username']);
       $d['password'] = trim ($d['password']);
       $d['password2'] = trim ($d['password2']);
       $d['email'] = trim ($d['email']);
       $d['firstname'] = trim ($d['firstname']);
       $d['surname'] = trim ($d['surname']);
       $d['city'] = trim ($d['city']);
	   $d['telephone'] = trim ($d['telephone']);
       $d['profesija'] = trim ($d['profesija']);
	   $d['get_info'] =$d['get_info']?1:0;
	   $d['user_country'] = trim ($d['user_country']);
		
       $d['license_number'] = trim ($d['license_number']);
       $d['working_company'] = trim ($d['working_company']);
       $d['workplace'] = trim ($d['workplace']);
       
       $d['sertifikats'] = trim ($d['sertifikats']);
       $d['sertifikats'] = mb_convert_encoding($d['sertifikats'], 'UTF-8');
		
		
		$d['working_place'] = trim ($d['working_place']);
		$d['working_specialty'] = trim ($d['working_specialty']);
		$d['working_address'] = trim ($d['working_address']);
       
       if ($d['profesija']=='Farmaceits' or $d['profesija']=='Farmaceita asistents')  
       { 
         $d['sertifikats'] = strtoupper($d['sertifikats']);
       }
       else 
       {
        unset($d['sertifikats']) ;
       }
       
       
     //  echo $d['sertifikats'].'!!';
       
           //]!='Farmaceits' && $d['profesija']!='Farmaceita asistents')  || !$d['profesija']    || !$d['city'] 
       if (!$d['username'] || !$d['password'] || !$d['password2'] || !$d['email'] || !$d['firstname'] || !$d['surname'] || (($d['profesija']=='Farmaceits' OR $d['profesija']=='Farmaceita asistents') && !$d['sertifikats']) )
       {
         $moodle_reg_error = __( 'Lūdzu aizpildiet visus obligātus laukus', 'moodle-synh' );
       }
       else if (!preg_match('/^[A-Za-z0-9]+$/i', $d['username']))
       {
         $moodle_reg_error = __( 'Lietotājvārds var saturēt tikai tikai burtus un ciparus!', 'moodle-synh' );
       }
       else if ($d['password'] <> $d['password2'])
       {
         $moodle_reg_error = __( 'Ievadītās paroles nesakrīt, lūdzu pārbaudiet!', 'moodle-synh' );
       }   
       else if (!$d['profesija'])
       {
         $moodle_reg_error = __( 'Lūdzu norādiet profesiju', 'moodle-synh' );
       }		
       else if ($d['profesija']=='Farmaceita asistents' && !preg_match("/^FA-\d{4}$/", $d['sertifikats']))
       {
         $moodle_reg_error = __( 'Sertifikāta numurs nav derīgs, ievadiet sertifikāta numuru formātā FA-XXXX', 'moodle-synh' );
       }
       else if ($d['profesija']=='Farmaceits' && !preg_match("/^F-\d{4}$/", $d['sertifikats']))
       {
         $moodle_reg_error = __( 'Sertifikāta numurs nav derīgs, ievadiet sertifikāta numuru formātā F-XXXX', 'moodle-synh' );
       }
       else if (!$d['agree'])
       {
         $moodle_reg_error = __( 'Lai turpinātu šīs vietnes izmantošanu, jums ir jāpiekrīt šai politikai.', 'moodle-synh' );
       }
	   else if (ICL_LANGUAGE_CODE=='lt' && !$d['license_number'] )
	   {
		 $moodle_reg_error = __( 'Input your license number', 'moodle-synh' ); 
	   }
	   else if (ICL_LANGUAGE_CODE=='lt' && (!$d['working_place'] || !$d['working_specialty'] || !$d['working_address']) )
	   {
		 $moodle_reg_error = __( 'Lūdzu aizpildiet visus obligātus laukus', 'moodle-synh' ); 
	   }		
	   else if (!$d['city'] )
	   {
		 $moodle_reg_error = __( 'Input city', 'moodle-synh' ); 
	   }	
	   else if (!$d['user_country'] )
	   {
		 $moodle_reg_error = __( 'Choose your country', 'moodle-synh' ); 
	   }		
	   else if (ICL_LANGUAGE_CODE=='lt' && !$d['working_company'] )
	   {
		 $moodle_reg_error = __( 'Input your company name', 'moodle-synh' ); 
	   }
	   else if (ICL_LANGUAGE_CODE=='lt' && !$d['workplace'] )
	   {
		 $moodle_reg_error = __( 'Input your workplace', 'moodle-synh' ); 
	   }
	   else if (ICL_LANGUAGE_CODE=='lt' && !$d['telephone'] )
	   {
		 $moodle_reg_error = __( 'Input your telephone', 'moodle-synh' ); 
	   }		
       else
       {
              $user = array(
                  "username" => strtolower($d['username']), // must be unique.
                  "password" => $d['password'],
                  "firstname" => ucwords(strtolower($d['firstname'])),
                  "lastname" => ucwords(strtolower($d['surname'])),
                  "email" => $d['email'],
                  "city" => ucwords(strtolower($d['city'])),
                  "country" => $d['user_country'],
				  "phone1" => $d['telephone'],
				  
				  "institution" => $d['working_place'],
				  "msn" => $d['working_specialty'],
				  "address" => $d['working_address'],
                  
                  "auth" => 'manual',
                  "customfields" => array ( // If you have custom fields in your system.
                      array(
                          "type" => (ICL_LANGUAGE_CODE == 'lv'?"speciality":"speciality_lt"),
                          "value" => trim($d['profesija'])
                          ),
                      array(
                          "type" => "certificate",
                          "value" => ($d['profesija']=='Farmaceits' or $d['profesija']=='Farmaceita asistents')?$d['sertifikats']:''
                          ),
                      array(
                          "type" => "gdpr_ip",
                          "value" => $_SERVER['REMOTE_ADDR']
                          ), 
                      array(
                          "type" => "gdpr_date",
                          "value" => date('d.m.Y H:i:s')
                          ),
                      array(
                          "type" => "get_info",
                          "value" => $d['get_info']
                          ),
                      array(
                          "type" => "licence",
                          "value" => $d['license_number']
                          )  ,
                      array(
                          "type" => "Working_company",
                          "value" => $d['working_company']
                          )  ,
                      array(
                          "type" => "Workplace_address",
                          "value" => $d['workplace']
                          )	,
                      array(
                          "type" => "Tel_number",
                          "value" => (ICL_LANGUAGE_CODE=='lt'?$d['telephone']:'')
                          )  					  
                      )
              
              );
     
            $access_url = woodle_get_settings( 'access_url', 'dc_woodle_general' );  
            $token = woodle_get_settings( 'ws_token', 'dc_woodle_general' ); 
$users = array($user); // must be wrapped in an array because it's plural.

$param = array("users" => $users); // the paramater to send
       
            $serverurl = $access_url . '/webservice/rest/server.php'. '?wstoken=' . 
                         $token . '&wsfunction=core_user_create_users';
 
            $restformat = 'json';
            $restformat = ($restformat == 'json')?'&moodlewsrestformat=' . 
                           $restformat:'';
                           

            $resp = $curl->post($serverurl . $restformat, $param);
		   //print_r($resp);
		   //exit();
            $resp_r = json_decode($resp, true);
            
$serverurl = $access_url . '/login/token.php?username='.$user['username'].'&password='.urlencode($user['password']) .'&service=moodle_mobile_app';
$resp1 = $curl->get($serverurl . $restformat);
$rest11 = json_decode($resp1, true);

$params=array();
$serverurl = $access_url . '/webservice/rest/server.php'. '?wstoken=' . $rest11['token'] . '&wsfunction=core_user_agree_site_policy ';
$resp1 = $curl->get($serverurl . $restformat);

            
            
           // print_r($resp_r);
		   //exit();
            if ($resp_r['errorcode']) {
            
            $moodle_reg_error = $resp_r['debuginfo']?$resp_r['debuginfo']:$resp_r['errorcode'];
            
            if (strpos($moodle_reg_error, 'Username already exists') !== false) { $moodle_reg_error = __( 'Šis lietotājvārds jau ir aizņemts, izvēlieties citu.', 'moodle-synh' ); }
            else if (strpos($moodle_reg_error, 'Email address is invalid') !== false) { $moodle_reg_error = __( 'E-pasta adrese nav derīga, ievadiet citu e-pasta adresi.', 'moodle-synh' ); }
            
            }
            else
            {
                        
                         $moodle_reg_error =  __( 'Paldies! Reģistrācija ir veiksmīgi pabeigta. Tagad Jums ir pieeja kursiem un produktu datu bāzei! Lai izmantot portālu, lūdzu, autorizēties.', 'moodle-synh' ); $ok=1;
                         
				         if (ICL_LANGUAGE_CODE=='lt')
                         $text = woodle_get_settings( 'ws_moodle_reg_txt_lt', 'dc_woodle_general' );
						 else $text = woodle_get_settings( 'ws_moodle_reg_txt', 'dc_woodle_general' );
				
                         $text = nl2br($text);
                         //site_url()
                         $text= str_replace('{{PORTAL_URL}}', '<b>'.site_url().'</b>',$text);
                         $text= str_replace('{{LOGIN_URL}}', '<a href="'.site_url().'/autorizacija/'.'">'.site_url().'/autorizacija/'.'</a>',$text);
                         
                         $headers = array('Content-Type: text/html; charset=UTF-8','From: '.woodle_get_settings( 'ws_moodle_email_name', 'dc_woodle_general' ).' <'.woodle_get_settings( 'ws_moodle_email', 'dc_woodle_general' ).'>');
                         
                         //wp_mail($d['email'], woodle_get_settings( 'ws_moodle_email_subject', 'dc_woodle_general' ), $text, $headers);
                        
                         
                         
                      $MAIN_CONTENT = file_get_contents (get_template_directory() . '/template-parts/emailer.html');
                      $content = str_replace("{{MAIN_CONTENT}}",$text,$MAIN_CONTENT);             
                       
                        $bg_path = get_template_directory_uri().'/template-parts/images/email_of_bg.jpg'; 
                        $content = str_replace("{{BG_PATH}}",$bg_path,$content);
                        
                        $logo_path = get_template_directory_uri().'/template-parts/images/logo.png';
                        $content = str_replace("{{LOGO_AND_LINK}}","<a href='".get_site_url()."' target='_blank'  title='AS Olainfarm'><img src='".$logo_path."' alt='AS Olainfarm'></a>",$content); 
				
				         if (ICL_LANGUAGE_CODE=='lt')
                         $subj = woodle_get_settings( 'ws_moodle_email_subject_lt', 'dc_woodle_general' );
						 else $subj = woodle_get_settings( 'ws_moodle_email_subject', 'dc_woodle_general' );
				
                        wp_mail($d['email'], $subj, $content, $headers);
                        
                       // echo $d['email'].'....'.$content;
                        
/*echo '<form style="display: none;" method="post" action="'.get_site_url().'/autorizacija/" name="hdLoginFrm" id="hdLoginFrm">
            <input type="hidden" name="username" value="'.$user['username'].'">
            <input type="hidden" name="password" value="'.$user['password'].'">
            <input type="hidden" name="login" value="1">
            </form>
            <script>document.getElementById(\'hdLoginFrm\').submit();</script>';    */
         
         //  echo 'ok';
         //  echo '<pre>';
          $restformat = ($restformat == 'json')?'&moodlewsrestformat=' . 
                         $restformat:'';
          
          $params=array();
          $serverurl = $access_url . '/login/token.php?username='.urlencode(trim(strtolower($user['username']))).'&password='.urlencode(trim($user['password'])).'&service=moodle_mobile_app';
          
         // echo "1  ".$serverurl.'<BR><BR>';
          $resp1 = $curl->get($serverurl . $restformat);
          $rest11 = json_decode($resp1, true);
          
          //echo '2<BR>';
          //print_r($rest11);
         //echo '<br><Br>';
          if($rest11['token'])
            {
                $_SESSION['moodle_user']['token']=$rest11['token'];
                $_SESSION['moodle_user']['username']=trim(strtolower($_POST['username']));
                
                $serverurl = $access_url . '/webservice/rest/server.php?wstoken='.$rest11['token'].'&wsfunction=core_user_get_users_by_field&field=username&values[0]='.trim(strtolower($_POST['username'])).'&moodlewsrestformat=json';

                $resp1 = $curl->get($serverurl);
                $array = (json_decode($resp1, true));
                
               // echo '3<BR>';
                //echo $serverurl.'<BR>';
                //print_r($resp1);
               // echo '<br><br>';
                $_SESSION['moodle_user'] = $array[0]; 
                
              $param_login = [
                  'user' => [
                      'username'  => urlencode(strtolower(trim($user['username']))), 
                  ]
              ];
                  $serverurl = $access_url . '/webservice/rest/server.php' . '?wstoken=' . $token . '&wsfunction=auth_userkey_request_login_url&moodlewsrestformat=json';
                  
                  $resp     = $curl->post($serverurl, $param_login);
                  $resp     = json_decode($resp);  
                  
                 // echo '4<br>';
                 // echo $serverurl.'<BR>';
                  //print_r($resp);
  
                  $g_link =  get_permalink( apply_filters( 'wpml_object_id', 11451, 'page', false, ICL_LANGUAGE_CODE ) );
                  wp_redirect($g_link);
                  exit;                             
            }
                
            
                                                
            }

                  
       }    
    
    }

  
  
}