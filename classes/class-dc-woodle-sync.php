<?php

class DC_Woodle_Sync {
	
	public $has_error;
	
	public $error_msg;
	
	public function __construct() {
		add_action( 'wp_loaded', array( &$this, 'sync' ) );
	}
    
    
    public function test123() {
       global $DC_Woodle;
       
       return 'hello';
    }

	
	/**
	 * Initiate sync process.
	 *
	 * @access public
	 * @return void
	 */
	public function sync() {
		global $DC_Woodle;

        //file_put_contents('./log_'.date("j.n.Y").'.log', "syn1\n\n", FILE_APPEND);
        
     
        

		if($_GET['mwsync']<>'1')  {
        
        $user_id = get_current_user_id();
		if ( $user_id <= 0 ) {
			return;
		}
		if ( 'POST' !== strtoupper( $_SERVER[ 'REQUEST_METHOD' ] ) ) {
			return;
		}
    
        }
         
        //file_put_contents('./log_'.date("j.n.Y").'.log', "syn2\n\n", FILE_APPEND);

		if ( empty( $_POST['dc_dc_woodle_sync_settings_name']['action'] ) || 'sync_courses_and_categories' !== $_POST['dc_dc_woodle_sync_settings_name']['action'] ) {
			return;
		}
        //file_put_contents('./log_'.date("j.n.Y").'.log', "syn3\n\n", FILE_APPEND);
           
		if($_GET['mwsync']<>'1')  {
		if( empty( $_POST['_wpnonce'] ) || ! wp_verify_nonce( $_POST['_wpnonce'], 'dc-sync_courses_and_categories' ) ) {
			return;
		}
        }
		  
		if ( empty( $_POST['dc_dc_woodle_sync_settings_name']['sync_now'] ) || $_POST['dc_dc_woodle_sync_settings_name']['sync_now'] == 'no' ) {
			return;
		}
		
		$access_url = woodle_get_settings( 'access_url', 'dc_woodle_general' );
       
		
		if( empty( $access_url ) ) {
			woodle_add_notice( '<strong>Synchronization can not be done.</strong> Moodle access url is not set. 
													<a href="'. admin_url( 'admin.php?page=dc-woodle-setting-admin' ) .'"> Set it now &nbsp;&raquo;</a>' );
			return;
		}
		
		$ws_token = woodle_get_settings( 'ws_token', 'dc_woodle_general' );
		
		if( empty( $ws_token ) ) {
			woodle_add_notice( '<strong>Synchronization can not be done.</strong> Moodle web service token is not set.
													<a href="'. admin_url( 'admin.php?page=dc-woodle-setting-admin' ) .'"> Set it now &nbsp;&raquo;</a>' );
			return;
		}

	if($_POST['dc_dc_woodle_sync_settings_name']['sync_category'] == 'yes')
    {		   $this->sync_specialties();
               $this->sync_categories();
            
    }        
    
    
    if( ! $DC_Woodle->ws_has_error ) {
    	if($_POST['dc_dc_woodle_sync_settings_name']['sync_course'] == 'yes')
    		$this->sync_courses();
    }
    
    if( ! $DC_Woodle->ws_has_error ) {
			woodle_add_notice( $DC_Woodle->ws_error_msg, 'updated' );
		} else {
			woodle_add_notice( $DC_Woodle->ws_error_msg, 'error' );
		}
	}
    
    
    private function sync_specialties()
    {
        global $DC_Woodle,$curl;
        
        $s_link = woodle_get_settings( 'ws_specialties_link', 'dc_woodle_general' );
        //echo 'aaaaa'.$s_link ;
        
        $get_spec = $curl->get($s_link);
        $get_spec_array = json_decode($get_spec, true);
        sort($get_spec_array,SORT_LOCALE_STRING);  
        
        foreach ($get_spec_array as $v)
          {
            $post_id = woodle_get_post_by_moodle_id($v, 'specialty' );

$args = array(
    'post_type'        => 'specialty' ,
	'meta_key'		=> '_specialty',
	'meta_value'	=> $v,
);


$find_by_meta = get_posts( $args ); 


/*print_r(array(
    'meta_key'   => '_specialty',
    'meta_value' => $v,
)); */
   

if(!$find_by_meta)
{
				   $args = array( 
                                    'post_title' => $v,
                                     'post_status'  => 'publish'
								);
                    $args['post_type'] = 'specialty';
					$post_id = wp_insert_post( $args );
                    
                    update_post_meta($post_id,'_specialty',$v);
}

      
            

          } 
        

        
    }
    
	
	/**
	 * Sync course categories from moodle.
	 *
	 * @access private
	 * @return void
	 */
	private function sync_categories() {
		global $DC_Woodle, $sitepress;
        
        $current_lang = $sitepress->get_current_language();
        $default_lang = $sitepress->get_default_language();
        $sitepress->switch_lang($default_lang);

   
       if($_GET['mwsync']=='1')
        $categories = woodle_moodle_core_function_callback( 'core_course_get_categories' );
       else
		$categories = woodle_moodle_core_function_callback( $DC_Woodle->moodle_core_functions['get_categories'] );
        
        //    echo $DC_Woodle->moodle_core_functions['get_categories'];  

        
        if( ! empty( $categories ) ) {
			foreach( $categories as $category ) {
            
            if ($category['name']){
                      $args = array(
                        'post_type'     => 'moodlecategory' ,
                      	'title'	=> $category['name'],
                        'post_status'  => 'publish',
                         'suppress_filters' => false,
                      );
                      $find_by_meta = get_posts( $args ); 
                      


                      if(count($find_by_meta)<1)
                      {
                      				   $args = array( 
                                                        'post_title' => $category['name'],
                                                        'post_status'  => 'publish'
                      								);
                                        $args['post_type'] = 'moodlecategory';
                      					$cat_id = wp_insert_post( $args );
                                                              
                      }
                   //   exit();
            
            }  
            }
        }


		
		if( ! $DC_Woodle->ws_has_error ) {

			$this->update_categories( $categories, 'course_cat', 'woodle_term' );
			$this->update_categories( $categories, 'product_cat', 'woocommerce_term' );
		}
	}
	
	/**
	 * Update moodle course categories in Wordpress site.
	 *
	 * @access private
	 * @param array $categories
	 * @param string $taxonomy
	 * @param string $meta_key
	 * @return void
	 */
	private function update_categories( $categories, $taxonomy, $meta_key ) {
    

		if( empty( $taxonomy ) || empty( $meta_key ) || ! taxonomy_exists( $taxonomy ) ) {
			return;
		}
        
    
		
		$category_ids = array();
		if( ! empty( $categories ) ) {
			foreach( $categories as $category ) {
				$term_id = woodle_get_term_by_moodle_id( $category['id'], $taxonomy, $meta_key );
				if( ! $term_id ) {
					$name = $category['name'];
					$description = $category['description'];
                    $description = ''.$category['visible'];
					$term = wp_insert_term( $name, $taxonomy,
																	array( 'description' =>  $description, 'slug' => "{$name} {$category['id']}" ) 
																);
					if( ! is_wp_error( $term ) ) {
						apply_filters( "woodle_add_{$meta_key}_meta", $term['term_id'], '_category_id', $category['id'], false );
						apply_filters( "woodle_add_{$meta_key}_meta", $term['term_id'], '_parent', $category['parent'], false );
						apply_filters( "woodle_add_{$meta_key}_meta", $term['term_id'], '_category_path', $category['path'], false );
					}
				} else {
        
          $category['description'] =  ''.$category['visible'];
					$term = wp_update_term( $term_id, $taxonomy,
																	array( 'name' => $category['name'], 'slug' => "{$category['name']} {$category['id']}", 'description' => $category['description'] )
																);
					if( ! is_wp_error( $term ) ) {
						apply_filters( "woodle_update_{$meta_key}_meta", $term['term_id'], '_parent', $category['parent'], '' );
						apply_filters( "woodle_update_{$meta_key}_meta", $term['term_id'], '_category_path', $category['path'], false );
					}
				}
				$category_ids[] = $category['id'];
			}
		}
		
		$terms = woodle_get_terms( $taxonomy );
		if( $terms ) {
			foreach( $terms as $term ) {
				$category_id = apply_filters( "woodle_get_{$meta_key}_meta", $term->term_id, '_category_id', true );
				if( in_array( $category_id, $category_ids ) ) {
					$parent = apply_filters( "woodle_get_{$meta_key}_meta", $term->term_id, '_parent', true );
					$parent_term_id = woodle_get_term_by_moodle_id( $parent, $taxonomy, $meta_key );
					wp_update_term( $term->term_id, $taxonomy, array( 'parent' => $parent_term_id ) );
				} else if( ! empty( $category_id ) ) {
					wp_delete_term( $term->term_id, $taxonomy );
				}
			}
		}
	}
	
	/**
	 * Sync courses from moodle.
	 *
	 * @access private
	 * @return void
	 */
	private function sync_courses() {
		global $DC_Woodle, $sitepress;
             

            
            
                
		$url = woodle_get_settings( 'access_url', 'dc_woodle_general' );
		$token = woodle_get_settings( 'ws_token', 'dc_woodle_general' );
    $ps = woodle_get_settings( 'ws_moodle_password', 'dc_woodle_general' );
		$request_url = $url . '/login/token.php?username='.woodle_get_settings( 'ws_moodle_login', 'dc_woodle_general' ).'&password='.urlencode($ps).'&service=moodle_mobile_app';
    

    $response = wp_remote_post( $request_url, array(
    'method'      => 'POST',
    'timeout'     => 360));
    //echo '<pre>';
    


    $response_arr = json_decode( $response['body'], true );
    $private_token = $response_arr['token'];
    

	if($_GET['mwsync']=='1')
    $courses = woodle_moodle_core_function_callback( 'core_course_get_courses_by_field' );
    else	
	$courses = woodle_moodle_core_function_callback( $DC_Woodle->moodle_core_functions['get_courses_full'] );
    
    if ($courses['courses'])  $courses =  $courses['courses'];
		       
		$this->update_posts( $courses, 'course', 'course_cat', 'woodle_term',$private_token );
		$this->update_posts( $courses, 'product', 'product_cat', 'woocommerce_term' );
	}
    
	
	/**
	 * Update moodle courses in Wordpress site.
	 *
	 * @access private
	 * @param array $courses
	 * @param string $post_type (default: null)
	 * @param string $taxonomy (default: null)
	 * @param string $meta_key (default: null)
	 * @return void
	 */
	private function update_posts( $courses, $post_type = '', $taxonomy = '', $meta_key = '', $private_token = '' ) {
  
    global $DC_Woodle,$sitepress;
    
        $current_lang = $sitepress->get_current_language();
        $default_lang = $sitepress->get_default_language();
        $sitepress->switch_lang($default_lang);

       //  set_current_user(1);
   
        if(!$_GET['mwsync'])
        {
    		if( empty( $post_type ) || ! post_type_exists( $post_type ) || empty( $taxonomy ) || ! taxonomy_exists( $taxonomy ) || empty( $meta_key ) ) {
    			return;
    		}
        }   
   
    	
		$course_ids = array();
		$create_wc_product = ( $post_type == 'product' ) ? woodle_get_settings( 'create_wc_product', 'dc_woodle_general' ) : '';
    
    
		$url = woodle_get_settings( 'access_url', 'dc_woodle_general' );
		$token = woodle_get_settings( 'ws_token', 'dc_woodle_general' );
     
             

		if( ! empty( $courses ) ) {
			foreach( $courses as $course ) {
      
      
      

      
     // if ($course['id']<>1){    
      //echo '<pre>';
      //print_r($course['overviewfiles']); 
      $course_image = $course['overviewfiles'][0]['fileurl'];
      $course_image = $course_image.'?token='.$private_token;
      
      //$course_image = '';


      
    foreach($course['customfields'] as $key2=>$value2)
    {
      

      if ($value2['shortname']=='points') $course['points'] = $value2['value'];
      else if ($value2['shortname']=='author') $course['author'] = $value2['value'];
      else if ($value2['shortname']=='points_author') $course['points_author'] = $value2['value'] ;
      else if ($value2['shortname']=='arst_farm') $course['arst_farm'] = $value2['value'] ;
      else if ($value2['shortname']=='arst_farm_2') $course['arst_farm_2'] = $value2['value'] ;
      else if ($value2['shortname']=='test_required') $course['test_required'] = $value2['value'] ;  
      else if ($value2['shortname']=='open_availability') $course['open_availability'] = $value2['value'] ; 
      else if ($value2['shortname']=='category') $course['category1'] = $value2['value'] ; 
      else if ($value2['shortname']=='category2') $course['category2'] = $value2['value'] ; 
      else if ($value2['shortname']=='course_lang') $course['course_lang'] = $value2['value'] ;
	  else if ($value2['shortname']=='lt_green_box') $course['lt_green_box'] = $value2['value'] ;
    }
				
	
    

                   if($course['category1']){
                                    $args = array(
                                        'post_type'     => 'moodlecategory' ,
                                    	'title'	=> $course['category1'],
                                        'post_status'  => 'publish',
                                        'suppress_filters' => false,
                                    );
                                    $find_by_meta = get_posts( $args ); 

                                    if(!$find_by_meta)
                                    {
                                    				   $args = array( 
                                                                      'post_title' => $course['category1'],
                                                                      'post_status'  => 'publish'
                                    								);
                                                        $args['post_type'] = 'moodlecategory';
                                    					$cat_id = wp_insert_post( $args );
                                    }
                                   //echo 'cat1 '.$course['category1'].' --- '.$cat_id.'<BR>';
                       //echo $course['id'].'<br>';              
					   //print_r($find_by_meta);
                      }  
                      
                   if($course['category2']){
                                    $args = array(
                                        'post_type'     => 'moodlecategory' ,
                                    	'title'	=> $course['category2'],
                                        'post_status'  => 'publish',
                                        'suppress_filters' => false,                                        
                                    );
                                    $find_by_meta = get_posts( $args ); 

                                    if(!$find_by_meta)
                                    {
                                    				   $args = array( 
                                                                      'post_title' => $course['category2'],
                                                                      'post_status'  => 'publish'
                                    								);
                                                       $args['post_type'] = 'moodlecategory';
                                    				   $cat_id = wp_insert_post( $args );
                                    }
                                   // echo 'cat2 '.$course['category2'].' --- '.$cat_id.'<BR>';
                                   //  print_r($find_by_meta);
                      }            //             
    
      // echo 'open_availability: '.$course['open_availability'].'<BR><BR>';

      
				if( $course['format'] == 'site' ) {
					continue;
				}
				
				$post_id = woodle_get_post_by_moodle_id( $course['id'], $post_type );	
				$post_status = 'publish';
				
				if( $post_type == 'product' && $post_id ) {
					$product = get_post( $post_id );
					$post_status = $product->post_status;
				}
				
				$args = array( 'post_title' => $course['fullname'],
											 'post_name'      => $course['shortname'],
											 'post_content'   => $course['summary'],
											 'post_status'    => $post_status
										 );
		$visibility = '';
    
    $lector_list='';    
    $lectors=array();     
		$request_url = $url . '/webservice/rest/server.php?wstoken='.$token.'&wsfunction=core_enrol_get_enrolled_users&moodlewsrestformat=json&courseid='.$course['id'];
    $response = wp_remote_post( $request_url, array(
    'method'      => 'POST',
    'timeout'     => 360) );
    $response_arr=array(); 
    $lectors_ok=0;
 
if ( is_wp_error( $response ) ) {
}
else {   
 if ( isset($response) )  
 {
  if(count(@$response['body']))
  $response_arr = json_decode( @$response['body'], true ); 
  
      for ($i=0;$i<count($response_arr); $i++)
      {
         $roles = $response_arr[$i]['roles'];
         for ($m=0;$m<count($roles);$m++)
         {
             if ($roles[$m]['shortname'] == 'editingteacher') $lectors[]=  $response_arr[$i]['fullname']?$response_arr[$i]['fullname']:$response_arr[$i]['firstname'].' '.$response_arr[$i]['lastname'];
         }
      }
                                
       $lector_list  = implode(";", $lectors);   
       $lectors_ok=1;
 }
}


            

				if( $post_id ) {
					$args['ID'] = $post_id;					
					$post_id = wp_update_post( $args );
					
                     if ($course['open_availability'] <> 'Lietuva' && $course['open_availability'] <> 'LT' && $course['open_availability'] <> 'Latvija' && $course['open_availability'] <> 'LV')
                     {
                        wp_delete_post( $post_id, false );
                        continue;
                     }					
                    
                    if ($course['open_availability'] == 'Lietuva' or $course['open_availability'] == 'LT')
                    {
                    $c_lang = 'lt';
                    } else $c_lang = 'lv';
                    
                    
                    
                    $my_post_language_details = apply_filters( 'wpml_post_language_details', NULL, $post_id ) ;
                    
                    if ($my_post_language_details['language_code']<>$c_lang)
                    {
                        wp_delete_post( $post_id, true );
                        continue;
                    }
                    
					
					if( $post_id ) {
						if( $post_type != 'product' ) {
                        
                        $post_thumbnail_id = get_post_thumbnail_id( woodle_get_post_by_moodle_id( $course['id'], $post_type ) );
                        
                       // $course_image = '';
                       // echo $course_image.'<BR><BR>aa'.$post_thumbnail_id = get_post_thumbnail_id( woodle_get_post_by_moodle_id( $course['id'], $post_type ) ) ;
                        
                        //no image in XML, but image exists in DB
                        if (!$course_image && $post_thumbnail_id)
                        {
                                delete_post_thumbnail($post_id);
                                wp_delete_attachment($post_id);
                        }
                        else
                        {
                        


                            if ($course_image)
                            {
                                
                                $get_timemodified = get_post_meta( $post_id, '_timemodified', true )  ;
                                //echo $get_timemodified.'  -  '.$course['overviewfiles'][0]['timemodified'];
                                //exit();
                                if($get_timemodified==$course['overviewfiles'][0]['timemodified'])
                                {
                                
                                }
                                else
                                {
                                      delete_post_meta(  $post_id, '_timemodified' );
                                      delete_post_thumbnail($post_id);
                                      wp_delete_attachment($post_id); 
                                      
                                      if ($course_image)
                                      {
                                            $course_image_array=array();
                                            $course_image_array['path'] = $course_image;
                                            $course_image_array['timemodified'] = $course['overviewfiles'][0]['timemodified'];
                                            $course_image_array['mimetype'] = $course['overviewfiles'][0]['mimetype'];
                                            $course_image_array['filename'] = $course['overviewfiles'][0]['filename']; 
                                            
                     
                                            
                                            $upload_dir       = wp_upload_dir(); // Set upload folder
                                            $image_data       = file_get_contents( $course_image_array['path']); // Get image data
                                            $unique_file_name = wp_unique_filename( $upload_dir['path'], $course_image_array['filename'] ); // Generate unique name
                                            $filename         = basename( $unique_file_name ); // Create image file name
                                            
                                            if( wp_mkdir_p( $upload_dir['path'] ) ) {
                                                $file = $upload_dir['path'] . '/' . $filename;
                                            } else {
                                                $file = $upload_dir['basedir'] . '/' . $filename;
                                            }
                                            file_put_contents( $file, $image_data );
                                            $wp_filetype = wp_check_filetype( $filename, null );
                                            
                                      		$attachment = array(
                                      			'post_mime_type' => $wp_filetype['type'],
                                      			'post_title'	 => sanitize_file_name( $filename ), // you may want something different here
                                      			'post_content'	 => '',
                                      			'post_status'	 => 'inherit'
                                      		);
                                  	
                                      		// Insert the attachment!
                                          $attach_id = wp_insert_attachment( $attachment, $file, woodle_get_post_by_moodle_id( $course['id'], $post_type ) );
                                          //$attach_id = media_sideload_image( $attachment, $file, woodle_get_post_by_moodle_id( $course['id'], $post_type ) );
                                          
                                      		require_once(ABSPATH . 'wp-admin/includes/image.php');
                                      		
                                          // Define attachment metadata
                                          $attach_data = wp_generate_attachment_metadata( $attach_id, $file );                   
                                          // Assign metadata to attachment
                                          wp_update_attachment_metadata( $attach_id, $attach_data );
                  
                                          set_post_thumbnail( $post_id, $attach_id );
                                          add_post_meta(  $post_id, '_thumbnail_id', $attach_id );
                                          add_post_meta(  $post_id, '_timemodified', $course_image_array['timemodified'] );                                                                       
                                      }                                       
                                                                             
                                }
                            }
                                
                        }
                        
                        

                        
                        
							$shortname = $course['shortname'];
							update_post_meta( $post_id, '_course_short_name', $shortname );
							update_post_meta( $post_id, '_course_idnumber', $course['id'] );
                            update_post_meta( $post_id, '_course_points', $course['points']?$course['points']:0 );
							update_post_meta( $post_id, '_course_startdate', $course['startdate'] );
							update_post_meta( $post_id, '_course_enddate', $course['enddate'] );
                            
              if( ! get_post_meta( $post_id, '_course_points_author' ) ) {
                add_post_meta( $post_id, '_course_points_author',$course['points_author'] );
              } else
              {
                update_post_meta( $post_id, '_course_points_author', $course['points_author'] );             
              }                           
                            
              
              if( ! get_post_meta( $post_id, 'post_views_count' ) ) {
              add_post_meta( $post_id, 'post_views_count', 0 );
              }
              if( ! get_post_meta( $post_id, '_course_lector' ) ) {
                if($lectors_ok==1)add_post_meta( $post_id, '_course_lector', $lector_list );
              }else
              {
                if($lectors_ok==1)update_post_meta( $post_id, '_course_lector', $lector_list );
              }
              
              if( ! get_post_meta( $post_id, 'test_required' ) ) {
                add_post_meta( $post_id, 'test_required', $course['test_required'] );
              }else
              {
                update_post_meta( $post_id, 'test_required', $course['test_required'] );
              }
              
              ////////////////////
              if( ! get_post_meta( $post_id, '_category_1' ) ) {
                add_post_meta( $post_id, '_category_1', '' );
              }
              if( ! get_post_meta( $post_id, '_category_2' ) ) {
                add_post_meta( $post_id, '_category_2', '' );
              }
							

               if ($course['open_availability'] == 'Lietuva' or $course['open_availability'] == 'LT')
				   	$sf=true;
			   else $sf = false;
							
                      $args1 = array(
                        'post_type'     => 'moodlecategory' ,
                      	'title'	=> $course['category1'],
                        'post_status' => 'publish',
						  'suppress_filters' => $sf,
                      );
							//if ($course['open_availability'] == 'Lietuva' or $course['open_availability'] == 'LT')
                    
                      $find_by_meta1 = get_posts( $args1 ); 
                      if ($sf) $lid1 = $find_by_meta1[0]->ID;
					  else
					  $lid1 = icl_object_id($find_by_meta1[0]->ID ,'post',false,'lv');
                      if(!$course['category1'])$lid1=0;
                      update_post_meta( $post_id, '_category_1', $lid1?$lid1:0 );

            
            
                      $args2 = array(
                        'post_type'     => 'moodlecategory' ,
                      	'title'	=> $course['category2'],
                        'post_status' => 'publish',
						  'suppress_filters' => $sf,
                      );
                      $find_by_meta2 = get_posts( $args2 ); 
                      if ($sf) $lid2 = $find_by_meta2[0]->ID;
					  else		
					   $lid2 = icl_object_id($find_by_meta2[0]->ID ,'post',false,'lv');
							
                      if(!$course['category2'])$lid2=0;
                      update_post_meta( $post_id, '_category_2', $lid2?$lid2:0 );
                      
                     // echo $course['id'].' -'.$post_id.' >>'.$course['category1'].'<< '.$lid1.'<BR><Br>';
                     // exit(); 
                      
       
              ////////////////////
              
              if( ! get_post_meta( $post_id, 'lt_green_box' ) ) {
                add_post_meta( $post_id, 'lt_green_box', $course['lt_green_box'] );
              }else
              {
                update_post_meta( $post_id, 'lt_green_box', $course['lt_green_box'] );
              }
							
              if( ! get_post_meta( $post_id, 'arst_farm' ) ) {
                add_post_meta( $post_id, 'arst_farm', $course['arst_farm'] );
              }else
              {
                update_post_meta( $post_id, 'arst_farm', $course['arst_farm'] );
              }
              
              if( ! get_post_meta( $post_id, 'arst_farm_2' ) ) {
                add_post_meta( $post_id, 'arst_farm_2', $course['arst_farm_2'] );
              }else
              {
                update_post_meta( $post_id, 'arst_farm_2', $course['arst_farm_2'] );
              }
                            
              if( ! get_post_meta( $post_id, 'course_lang' ) ) {
                add_post_meta( $post_id, 'course_lang', $course['course_lang'] );
              }else
              {
                update_post_meta( $post_id, 'course_lang', $course['course_lang'] );
              }
              
                            
						}
						
						update_post_meta( $post_id, '_category_id', (int) $course['categoryid'] );
						update_post_meta( $post_id, '_visibility',  ( $course['visible'] ) ? 'visible' : 'hidden' );
						
						if( $post_type == 'product' ) {
							update_post_meta( $post_id, '_virtual', 'yes' );
							update_post_meta( $post_id, '_sold_individually', 'yes' );
							update_post_meta( $post_id, '_course_startdate', $course['startdate'] );
							update_post_meta( $post_id, '_course_enddate', $course['enddate'] );
						}
					}
				}	else if( $post_type != 'product' || $create_wc_product == 'yes' ) {
                
                   // echo  $course_image.'aaa';
                    
          
                  if ($course['open_availability'] <> 'Lietuva' && $course['open_availability'] <> 'LT' && $course['open_availability'] <> 'Latvija' && $course['open_availability'] <> 'LV')
                  continue;            
 
        
                    // $course['open_availability']
                  //  exit();
					$args['post_type'] = $post_type;
					$post_id = wp_insert_post( $args );
                    
    
                    if ($course['open_availability'] == 'Lietuva' or $course['open_availability'] == 'LT')
                    {
                        $def_trid = $sitepress->get_element_trid($post_id);
                        $sitepress->set_element_language_details($post_id, 'post_course', $def_trid, 'lt');
                    }
                    
                    
                    //exit();
					
					if( $post_id ) {
						if( $post_type != 'product' ) {
              add_post_meta( $post_id, '_course_points', $course['points']?$course['points']:0 );
							add_post_meta( $post_id, '_course_short_name', $course['shortname'] );
							add_post_meta( $post_id, '_course_idnumber', $course['idnumber'] );
              if($lectors_ok==1) add_post_meta( $post_id, '_course_lector', $lector_list );
              add_post_meta( $post_id, 'post_views_count', 0 );
              add_post_meta( $post_id, 'test_required', $course['test_required'] );
              add_post_meta( $post_id, 'arst_farm', $course['arst_farm'] );
              add_post_meta( $post_id, 'arst_farm_2', $course['arst_farm_2'] );
               add_post_meta( $post_id, 'course_lang', $course['course_lang'] );
               add_post_meta( $post_id, 'lt_green_box', $course['lt_green_box'] );
                    if ($course_image)
                    {
                          $course_image_array=array();
                          $course_image_array['path'] = $course_image;
                          $course_image_array['timemodified'] = $course['overviewfiles'][0]['timemodified'];
                          $course_image_array['mimetype'] = $course['overviewfiles'][0]['mimetype'];
                          $course_image_array['filename'] = $course['overviewfiles'][0]['filename']; 
                          
   
                          
                          $upload_dir       = wp_upload_dir(); // Set upload folder
                          $image_data       = file_get_contents( $course_image_array['path']); // Get image data
                          $unique_file_name = wp_unique_filename( $upload_dir['path'], $course_image_array['filename'] ); // Generate unique name
                          $filename         = basename( $unique_file_name ); // Create image file name
                          
                          if( wp_mkdir_p( $upload_dir['path'] ) ) {
                              $file = $upload_dir['path'] . '/' . $filename;
                          } else {
                              $file = $upload_dir['basedir'] . '/' . $filename;
                          }
                          file_put_contents( $file, $image_data );
                          $wp_filetype = wp_check_filetype( $filename, null );
                          
                    		$attachment = array(
                    			'post_mime_type' => $wp_filetype['type'],
                    			'post_title'	 => sanitize_file_name( $filename ), // you may want something different here
                    			'post_content'	 => '',
                    			'post_status'	 => 'inherit'
                    		);
                	
                    		// Insert the attachment!
                        $attach_id = wp_insert_attachment( $attachment, $file, woodle_get_post_by_moodle_id( $course['id'], $post_type ) );
                        //$attach_id = media_sideload_image( $attachment, $file, woodle_get_post_by_moodle_id( $course['id'], $post_type ) );
                        
                    		require_once(ABSPATH . 'wp-admin/includes/image.php');
                    		
                        // Define attachment metadata
                        $attach_data = wp_generate_attachment_metadata( $attach_id, $file );                   
                        // Assign metadata to attachment
                        wp_update_attachment_metadata( $attach_id, $attach_data );

                        set_post_thumbnail( $post_id, $attach_id );
                        add_post_meta(  $post_id, '_thumbnail_id', $attach_id );
                        add_post_meta(  $post_id, '_timemodified', $course_image_array['timemodified'] );                                                                       
                    }              
              
              
						}
                        
              ////////////////////
              if( ! get_post_meta( $post_id, '_category_1' ) ) {
                add_post_meta( $post_id, '_category_1', '' );
              }
              if( ! get_post_meta( $post_id, '_category_2' ) ) {
                add_post_meta( $post_id, '_category_2', '' );
              }

               if ($course['open_availability'] == 'Lietuva' or $course['open_availability'] == 'LT')
				   	$sf=true;
			   else $sf = false;
						
                      $args1 = array(
                        'post_type'     => 'moodlecategory' ,
                      	'title'	=> $course['category1'],
                        'post_status' => 'publish',
						    'suppress_filters' => $sf,
                      );
                      $find_by_meta1 = get_posts( $args1 ); 
                      $lid1 = icl_object_id($find_by_meta1[0]->ID ,'post',false,'lv');
                      if(!$course['category1'])$lid1=0;
                      update_post_meta( $post_id, '_category_1', $lid1?$lid1:0 );

            
            
                      $args2 = array(
                        'post_type'     => 'moodlecategory' ,
                      	'title'	=> $course['category2'],
                        'post_status' => 'publish',
						    'suppress_filters' => $sf,
                      );
                      $find_by_meta2 = get_posts( $args2 ); 
                      $lid2 = icl_object_id($find_by_meta2[0]->ID ,'post',false,'lv');
                      if(!$course['category2'])$lid2=0;
                      update_post_meta( $post_id, '_category_2', $lid2?$lid2:0 ); 
                      
                     // echo $course['id'].' -'.$post_id.' >>'.$course['category1'].'<< '.$lid1.'<BR><Br>';
                     // exit(); 
                      
       
              ////////////////////                 
						
						add_post_meta( $post_id, '_course_id', (int) $course['id'] );
						add_post_meta( $post_id, '_category_id', (int) $course['categoryid'] );
						add_post_meta( $post_id, '_visibility', $visibility = ( $course['visible'] ) ? 'visible' : 'hidden' );
                        
							add_post_meta( $post_id, '_course_short_name', $shortname );
							add_post_meta( $post_id, '_course_idnumber', $course['id'] );
                            add_post_meta( $post_id, '_course_points', $course['points']?$course['points']:0 );
							add_post_meta( $post_id, '_course_startdate', $course['startdate'] );
							add_post_meta( $post_id, '_course_enddate', $course['enddate'] );
                            add_post_meta( $post_id, '_course_points_author', $course['points_author'] );
                                                    
						
						if( $post_type == 'product' ) {
							add_post_meta( $post_id, '_sku', 'course-' . $course['id']);
							add_post_meta( $post_id, '_virtual', 'yes' );
							add_post_meta( $post_id, '_sold_individually', 'yes' );
						}
					}
                    
                    
				}

				$course_ids[$course['id']] = $course['categoryid'];
			}
		}
		
		$posts = woodle_get_posts( array( 'post_type' => $post_type ) );
		if( $posts ) {
			foreach( $posts as $post ) {
				$course_id = get_post_meta( $post->ID, '_course_id', true );
				if( array_key_exists( $course_id, $course_ids ) ) {
					$term_id = woodle_get_term_by_moodle_id( $course_ids[$course_id], $taxonomy, $meta_key );
					wp_set_post_terms( $post->ID, $term_id, $taxonomy );
				} else if( ! empty( $course_id ) ) {
					wp_delete_post( $post->ID, false );
				}
			}
		}
	}
}