<?php

class DC_Woodle_Posttype {

  private $labels = array();
	public $course;

	public function __construct() {
		global $DC_Woodle;

		$this->setup_post_type_labels_base();
		$this->setup_course_post_type();
		
		$this->setup_post_type_labels_base_specialty();
		$this->setup_specialty_post_type();   
        
		$this->setup_post_type_labels_base_country();
		$this->setup_country_post_type();  
		
		$this->setup_post_type_labels_base_moodlecategory();
		$this->setup_moodlecategory_post_type();  

		$this->load_class( 'posttype-course' );
		$this->course = new DC_Woodle_Posttype_Course();
		
		if( is_admin() ) {
			$this->load_class( 'posttype-product' );
			$this->course = new DC_Woodle_Posttype_Product();
		}
		
		if ( is_admin() ) {
			global $pagenow;
			if ( ( $pagenow == 'post.php' || $pagenow == 'post-new.php' ) ) {
				add_filter( 'post_updated_messages', array( &$this, 'setup_post_type_messages' ) );
			}
		}
    

    
	}

	public function setup_course_post_type() {
		global $DC_Woodle;

		$args = array(
			'labels'             => $this->create_post_type_labels( 'course', $this->labels['course']['singular'], $this->labels['course']['plural'], $this->labels['course']['menu'] ),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'query_var'          => true,
			'rewrite'            => true,
			'map_meta_cap'       => true,
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => 25,
			'menu_icon'          => 'dashicons-welcome-learn-more',
			'supports'           => array( 'title', 'editor' ),
			'capability_type' 	 => 'post',
      'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
			'capabilities'			 => array( 'create_posts'	=> false,
																		 'delete_posts' => true , 
                                      'edit_posts' => true  
																	 ),
      'taxonomies' => array('post_tag')
		);
    
    
    

		register_post_type( 'course', $args );
    // flush_rewrite_rules();
	}
  

	public function setup_moodlecategory_post_type() {
		global $DC_Woodle;

		$args = array(
			'labels'             => $this->create_post_type_labels( 'moodlecategory', $this->labels['moodlecategory']['singular'], $this->labels['moodlecategory']['plural'], $this->labels['moodlecategory']['menu'] ),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'query_var'          => true,
			'rewrite'            => true,
			'map_meta_cap'       => true,
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => 25,
			'menu_icon'          => 'dashicons-welcome-learn-more',
			'supports'           => array( 'title', 'editor' ),
			'capability_type' 	 => 'post',
            'supports'              => array( 'title'),
            
		/*	'capabilities'			 => array( 
                'create_posts'	=> true,
    			'delete_posts' => true , 
                'edit_posts' => true ,
                'edit_published_post'  => true ,
                'edit' => true ,
                'edit_post' => true ,
                'edit_others_posts'      => true,
             ),   */
      //'taxonomies' => array('post_tag')
		);
    


	 register_post_type( 'moodlecategory', $args );
     flush_rewrite_rules();
	}    
    
	
  
	private function create_post_type_labels( $token, $singular, $plural, $menu ) {
	  global $DC_Woodle;
	  
		$labels = array(
			'name'               => sprintf( _x( '%s', 'post type general name', 'moodle-synh' ), $plural ),
			'singular_name'      => sprintf( _x( '%s', 'post type singular name', 'moodle-synh' ), $singular ),
      
			'add_new'            => sprintf( _x( 'Add New %s', $token, 'moodle-synh' ), $singular ),
			'add_new_item'       => sprintf( __( 'Add New %s', 'moodle-synh' ), $singular ),
			'edit_item'          => sprintf( __( 'Edit %s', 'moodle-synh' ), $singular ),
			'new_item'           => sprintf( __( 'New %s', 'moodle-synh' ), $singular ),
			'all_items'          => sprintf( __( '%s', 'moodle-synh' ), $plural ),
			'view_item'          => sprintf( __( 'View %s', 'moodle-synh' ), $singular ),
			'search_items'       => sprintf( __( 'Search %s', 'moodle-synh' ), $plural ),
			'not_found'          => sprintf( __( 'No %s found', 'moodle-synh' ), strtolower( $plural ) ),
			'not_found_in_trash' => sprintf( __( 'No %s found in Trash', 'moodle-synh' ), strtolower( $plural ) ),
			'parent_item_colon'  => '',
			'menu_name'          => sprintf( __( '%s', 'moodle-synh' ), $menu )
		);

		return $labels;
	}
	
	public function setup_specialty_post_type() {
		global $DC_Woodle;

		$args = array(
			'labels'             => $this->create_post_type_labels( 'specialty', $this->labels['specialty']['singular'], $this->labels['specialty']['plural'], $this->labels['specialty']['menu'] ),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'query_var'          => true,
			'rewrite'            => true,
			'map_meta_cap'       => true,
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => 25,
			'menu_icon'          => 'dashicons-welcome-learn-more',
			'supports'           => array( 'title', 'editor' ),
			'capability_type' 	 => 'post',
            'supports'              => array( 'title'),
            
		/*	'capabilities'			 => array( 
                'create_posts'	=> true,
    			'delete_posts' => true , 
                'edit_posts' => true ,
                'edit_published_post'  => true ,
                'edit' => true ,
                'edit_post' => true ,
                'edit_others_posts'      => true,
             ),   */
      //'taxonomies' => array('post_tag')
		);
    


	 register_post_type( 'specialty', $args );
     flush_rewrite_rules();
	}    
    
	public function setup_country_post_type() {
		global $DC_Woodle;

		$args = array(
			'labels'             => $this->create_post_type_labels( 'country', $this->labels['country']['singular'], $this->labels['country']['plural'], $this->labels['country']['menu'] ),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'query_var'          => true,
			'rewrite'            => true,
			'map_meta_cap'       => true,
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => 25,
			'menu_icon'          => 'dashicons-welcome-learn-more',
			'supports'           => array( 'title', 'editor' ),
			'capability_type' 	 => 'post',
            'supports'              => array( 'title','excerpt'),
            
		/*	'capabilities'			 => array( 
                'create_posts'	=> true,
    			'delete_posts' => true , 
                'edit_posts' => true ,
                'edit_published_post'  => true ,
                'edit' => true ,
                'edit_post' => true ,
                'edit_others_posts'      => true,
             ),   */
      //'taxonomies' => array('post_tag')
		);
    


	 register_post_type( 'country', $args );
     flush_rewrite_rules();
	}  

	public function setup_post_type_messages ( $messages ) {
		global $post, $post_ID, $DC_Woodle;

		$messages['course'] = $this->create_post_type_messages( 'course' );

		return $messages;
	}

	private function create_post_type_messages( $post_type ) {
		global $post, $post_ID, $DC_Woodle;

		if ( ! isset( $this->labels[ $post_type ] ) ) {
			return array();
		}

		$messages = array(
			0  => '',
			1  => sprintf( __( '%s updated.' ), esc_attr( $this->labels[ $post_type ]['singular'] ) ),
			2  => __( 'Custom field updated.', 'moodle-synh' ),
			3  => __( 'Custom field deleted.', 'moodle-synh' ),
			4  => sprintf( __( '%s updated.', 'moodle-synh' ), esc_attr( $this->labels[ $post_type ]['singular'] ) ),
			5  => isset( $_GET['revision'] ) ? sprintf( __( '%2$s restored to revision from %1$s', 'moodle-synh' ), 
																											 wp_post_revision_title( (int) $_GET['revision'], false ), 
																											 esc_attr( $this->labels[ $post_type ]['singular'] ) ) : false,
			6  => sprintf( __( '%2$s published.' ), esc_url( get_permalink( $post_ID ) ), esc_attr( $this->labels[ $post_type ]['singular'] ) ),
			7  => sprintf( __( '%s saved.', 'moodle-synh' ), esc_attr( $this->labels[ $post_type ]['singular'] ) ),
			8  => sprintf( __( '%2$s submitted.', 'moodle-synh' ), esc_url( add_query_arg( 'preview', 'true', get_permalink( $post_ID ) ) ), 
													esc_attr( $this->labels[ $post_type ]['singular'] ) ),
			9  => sprintf( __( '%s scheduled for: <strong>%1$s</strong>.', 'moodle-synh' ),
													date_i18n( __( ' M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink( $post_ID ) ), 
													strtolower( esc_attr( $this->labels[ $post_type ]['singular'] ) ) ),
			10 => sprintf( __( '%s draft updated.', 'moodle-synh' ), esc_attr( $this->labels[ $post_type ]['singular'] ) ),
		);

		return $messages;
	}

	private function setup_post_type_labels_base() {
	  global $DC_Woodle;
	  
		$this->labels['course'] = array( 'singular' => __( 'Course', 'moodle-synh' ),
																		 'plural' => __( 'Courses', 'moodle-synh' ), 
																		 'menu' => __( 'Courses', 'moodle-synh' ) );
	}
	
	private function setup_post_type_labels_base_specialty() {
	  global $DC_Woodle;
	  
		$this->labels['specialty'] = array( 'singular' => __( 'Specialty', 'moodle-synh' ),
																		 'plural' => __( 'Specialties', 'moodle-synh' ), 
																		 'menu' => __( 'Specialties', 'moodle-synh' ) );
	}
    
	private function setup_post_type_labels_base_country() {
	  global $DC_Woodle;
	  
		$this->labels['country'] = array( 'singular' => __( 'Country', 'moodle-synh' ),
																		 'plural' => __( 'Countries', 'moodle-synh' ), 
																		 'menu' => __( 'Countries', 'moodle-synh' ) );
	}
	
	private function setup_post_type_labels_base_moodlecategory() {
	  global $DC_Woodle;
	  
		$this->labels['moodlecategory'] = array( 'singular' => __( 'Category', 'moodle-synh' ),
																		 'plural' => __( 'Categories', 'moodle-synh' ), 
																		 'menu' => __( 'Categories', 'moodle-synh' ) );
	}
	
	/**
	 * Load class file
	 *
	 * @access public
	 * @param string $class_name (default: null)
	 * @return void
	 */
	public function load_class($class_name = '') {
		global $DC_Woodle;
		
		if ('' != $class_name && '' != $DC_Woodle->token) {
			require_once ('posttypes/class-' . esc_attr($DC_Woodle->token) . '-' . esc_attr($class_name) . '.php');
		}
	}
}