<?php
class DC_Woodle_Cron {

	public function __construct() 
  {
             //add_filter('cron_schedules', array($this, 'cron_time_intervals'));
             //add_action( 'wp',  array($this, 'cron_scheduler'));
             //add_action( 'cast_my_spell', array( $this, 'import_moodle_cr' ) ); 
	
	}
  
  
        public function cron_time_intervals($schedules)
        {
            $schedules['hour_1'] = array(
                'interval' => 3600,
                'display' => 'Once an hour'
            );

            return $schedules;
        }

        function cron_scheduler() {
        
            if (wp_next_scheduled ( 'cast_my_spell' )) 
            {
               wp_clear_scheduled_hook( 'cast_my_spell' );
             }
         
            if ( ! wp_next_scheduled( 'cast_my_spell' ) ) {
                wp_schedule_event( time(), 'hour_1', 'cast_my_spell');
            }
        }

        function import_moodle_cr(){
			
	 global $DC_Woodle;	
			
	 if ( isset($DC_Woodle) ) {
		 
            set_current_user(1);

            $_SERVER[ 'REQUEST_METHOD' ] = 'POST';
            $_POST['_wpnonce'] = wp_create_nonce( 'dc-sync_courses_and_categories' );
            $_POST['dc_dc_woodle_sync_settings_name']['action'] = 'sync_courses_and_categories';
            $_POST['dc_dc_woodle_sync_settings_name']['sync_now'] = 'yes';
			
			$_POST['dc_dc_woodle_sync_settings_name']['sync_course'] = 'yes';
			$_POST['dc_dc_woodle_sync_settings_name']['sync_category'] = 'yes';

			$DC_Woodle->load_class('sync');
			$DC_Woodle->sync = new DC_Woodle_Sync();

			$DC_Woodle->sync->sync();


        }
			
              	/*$recepients = 'marina@integra.lv';
              	$subject = 'Hello from your Cron Job (OF)';
              	$message = 'This is a test mail sent by WordPress automatically as per your schedule.';
              	  
              	// let's send it 
              	mail($recepients, $subject, $message);
                */
                
//$log  = "synh";
//file_put_contents('./log_'.date("j.n.Y").'.log', $log, FILE_APPEND);    
                
                
        }
        
        
}