<?php
class DC_Woodle_shortcode {


	public function __construct() {
    	add_shortcode('moowoodle', array( &$this, 'moowoodle_handler') );
        add_shortcode('moowoodle_login',array( &$this, 'moowoodle_login_handler') );
        add_shortcode('moowoodle_header',array( &$this, 'moowoodle_header_handler') );
        add_shortcode('moowoodle_register',array( &$this, 'moowoodle_registration_handler') );
        add_shortcode('moodle_hello',array( &$this, 'moowoodle_hello_handler') );
      
		if ( current_user_can('edit_posts') &&  current_user_can('edit_pages') ) {
		    add_filter('mce_external_plugins',array( &$this, 'moowoodle_add_plugin') );
		}

	}
  
public function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
    $sort_col = array();
    foreach ($arr as $key=> $row) {
        $sort_col[$key] = $row[$col];
    }

    array_multisort($sort_col, $dir, $arr);
}


 public function moowoodle_hello_handler( $atts, $content = null ) 
  {
	global $DC_Woodle;
   
    ob_start();
     
    if ($_SESSION['moodle_user'] && $_SESSION['moodle_user']['firstname'])
    {
      echo $_SESSION['moodle_user']['firstname'].' '.$_SESSION['moodle_user']['lastname'];
    }
    else if ($_SESSION['moodle_user'] && !$_SESSION['moodle_user']['firstname'] && $_SESSION['moodle_user']['fullname'])
    {
      echo $_SESSION['moodle_user']['fullname'];
    }    
    else
    {
      echo  __( 'portāla apmeklētājs', 'moodle-synh' );
    }
    
     return ob_get_clean();
    
  }

	public function moowoodle_header_handler( $atts, $content = null ) {
		global $DC_Woodle;
  if ( is_admin()){
	return;
}
    ?>
    
    <?php
  }
  

  
  public function moowoodle_registration_handler( $atts, $content = null ) {
    global $DC_Woodle, $curl, $COUNTRY, $PRO, $moodle_reg_error, $moodle_d_array;
    $error = '';
    ob_start();
    
    $d = $moodle_d_array;

    ?>
<main class="col-xs-12 " style="padding:0px !important; margin:0px !important;">		
<article id="post-12326" class="post-12326 page type-page status-publish hentry" >
	<div class="entry-content">
		<div class="woocommerce"><div class="woocommerce-notices-wrapper"></div>
    
<?php if ($moodle_reg_error) { ?>
<div class="woocommerce-notices-wrapper">
 <ul class="woocommerce-<?php if ($ok==1){?>info<?php } else { ?>error<?php } ?>" role="alert">                                          
			<li>
			<strong  ><?php echo $moodle_reg_error; ?></strong></li>
	</ul>
</div>
<?php } ?>

       
				<div class=" regform" >
					<div class="row">
            <div class="col-lg-2"></div>
						<div class="col-lg-8">
    
    <center><h2><?php echo __( 'KONTA IZVEIDE portālā “Open OlainFarm” ', 'moodle-synh' );  ?></h2></center>            
		<form class="woocommerce-form woocommerce-form-login login" method="post">
        
        <br><font style="font-size: 1.2em; margin: 0 0 20px 0;"><?php echo __( 'Lai Jūs varētu piedalīties seminārā/ vebinārā vai citā akciju sabiedrības “Olainfarm” (turpmāk - Olainfarm) nodrošinātajā pasākumā (turpmāk - Pasākums), Jums ir jāizveido konts portālā veselības aprūpes speciālistiem “Open OlainFarm”.<br>
Izveidojot KONTU, Jums tiks nodrošināta iespēja:<br>
&nbsp;&bull;&nbsp;reģistrēties uz izvēlēto Pasākumu,<br>
&nbsp;&bull;&nbsp;saņemt kontā apliecinājumu par dalību Pasākumā,<br>
&nbsp;&bull;&nbsp;saņemt aktuālu informāciju veselības aprūpes speciālistiem.', 'moodle-synh' );  ?></font>
         <BR><BR>

			<p class="fheader"><?php echo __( 'Lūdzu, sniedziet turpmāk norādīto informāciju konta izveidošanai:', 'moodle-synh' );  ?></p>
			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="username"><b><?php echo __( 'Lietotāja vārds', 'moodle-synh' );  ?></b>&nbsp;<span class="required">*</span></label>
				<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" maxlength="50" name="username" id="username" autocomplete="username" value="<?php echo $d['username']; ?>" />			</p>
			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="password"><b><?php echo __( 'Parole', 'moodle-synh' );  ?></b>&nbsp;<span class="required">*</span></label>
				<input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password"  value="<?php echo $d['password']; ?>" />
        <br> <i><?php echo __( 'Parolei jāsatur vismaz 6 simboli, no kuriem vismaz 1 mazais burts', 'moodle-synh' );  ?></i>
        <br>
			</p>
      
			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="password"><b><?php echo __( 'Paroles apstiprināšana', 'moodle-synh' );  ?></b>&nbsp;<span class="required">*</span></label>
				<input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password2" id="password2"  value="<?php echo $d['password2']; ?>" />			
            <br> 
            <i><?php echo __( 'Paroli apstipriniet, to ierakstot atkārtoti.', 'moodle-synh' );  ?></i>
            <br>
            </p>
                  
      
			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="firstname"><b><?php echo __( 'Jūsu vārds', 'moodle-synh' );  ?></b>&nbsp;<span class="required">*</span></label>
				<input class="woocommerce-Input woocommerce-Input--text input-text txtvalidate" type="text" maxlength="50" name="firstname" id="firstname" value="<?php echo $d['firstname']; ?>" />
			</p> 
			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="surname"><b><?php echo __( 'Jūsu uzvārds', 'moodle-synh' );  ?></b>&nbsp;<span class="required">*</span></label>
				<input class="woocommerce-Input woocommerce-Input--text input-text txtvalidate" maxlength="50" type="text" name="surname" id="surname" value="<?php echo $d['surname']; ?>" />
			</p> 
			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="city"><b><?php echo __( 'Pilsēta, kurā Jūs praktizējat', 'moodle-synh' );  ?></b>&nbsp;<span class="required">*</span></label>
				<input class="woocommerce-Input woocommerce-Input--text input-text" maxlength="50" type="text" name="city" id="city" value="<?php echo $d['city']; ?>" />
			</p>
			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="user_country"><b><?php echo __( 'Valsts', 'moodle-synh' );  ?></b>&nbsp;<span class="required">*</span></label>
				<select class="select2-selection__rendered" name="user_country" id='user_country'>
        <option value=""><?php echo __( 'Izvēlēties valsti...', 'moodle-synh' );  ?></option>
<?php
 $query_c = new WP_Query( array(
    'post_type'         => 'country',
    'post_status'       => 'publish',
    'posts_per_page' => '50000',
    'orderby' => 'title',
    'order' => 'ASC'
) );

if ( $query_c->have_posts() ) {
    while ( $query_c->have_posts() ) {
        $query_c->the_post();
        // display content
        if($query_c->post->post_title){
        $original_ID = icl_object_id( $query_c->post->ID, 'post', false, 'lv' );
        $original_title = get_the_title( $original_ID );
        ?>
        <option value="<?php echo $query_c->post->post_excerpt;  ?>" <?php if ($d['user_country']==$query_c->post->post_excerpt) echo "selected"; ?> ><?php echo $query_c->post->post_title;  ?></option>
        <?php
        }
    }
}
else {
    // display when no posts found
}

wp_reset_postdata(); 


?>        

        </select>
			</p>
<?php
ini_set('intl.default_locale', 'lv-LV');      
setlocale(LC_ALL, "en_US.UTF-8");
?>            
			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="profesija"><b><?php echo __('Jūsu specialitāte', 'moodle-synh' );  ?></b>&nbsp;<span class="required">*</span></label>
				<select class="select2-selection__rendered" name="profesija" <?php if (ICL_LANGUAGE_CODE == 'lv') { ?> onchange="if(this.value=='Farmaceits' || this.value=='Farmaceita asistents') {document.getElementById('cert_id').style.display='block';} else {document.getElementById('cert_id').style.display='none';}" <?php }?> >
        <option value=""><?php echo __( 'Izvēlēties specialitāti...', 'moodle-synh' );  ?></option>
        
        <?php
        
       if (ICL_LANGUAGE_CODE == 'lv')
       $s_link = woodle_get_settings( 'ws_specialties_link', 'dc_woodle_general' );
       else if (ICL_LANGUAGE_CODE == 'lt')
       $s_link = woodle_get_settings( 'ws_specialties_link_lt', 'dc_woodle_general' );       
       
        $get_spec = $curl->get($s_link);
       $get_spec_array = json_decode($get_spec, true);
        asort($get_spec_array);
        
              
          foreach ($get_spec_array as $v)
         {
              ?>
              <option value="<?php echo $v;  ?>" <?php if ($d['profesija']==$v) echo "selected"; ?> ><?php echo $v;  ?></option>
              <?php
          }
        ?>
        </select>
			</p>
			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide" id="cert_id" <?php if (!$d or ($d['profesija']!='Farmaceits' && $d['profesija']!='Farmaceita asistents') ) { ?>style="display: none;"
      <?php } ?> >
				<label for="email"><?php echo __( 'Sertifikāta numurs', 'moodle-synh' );  ?> <i>(<?php echo __( 'Farmaceitiem - F-XXXX, Farmaceita asistentiem FA-XXXX formātā', 'moodle-synh' );  ?>)</i>&nbsp;<span class="required">*</span></label>
				<input class="woocommerce-Input woocommerce-Input--text input-text" style="text-transform:uppercase" type="text" name="sertifikats" id="sertifikats" value="<?php echo $d['sertifikats']; ?>" />
			</p>                        
                        
			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="email"><b><?php echo __( 'E-pasta adrese', 'moodle-synh' );  ?></b>&nbsp;<span class="required">*</span></label>
				<input class="woocommerce-Input woocommerce-Input--text input-text" type="text" name="email" id="email" value="<?php echo $d['email']; ?>" />
			</p>       
			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="telephone"><b><?php echo __( 'Tālruņa numurs', 'moodle-synh' );  ?></b>&nbsp;<?php if (ICL_LANGUAGE_CODE=='lt') { ?><span class="required">*</span><?php } ?></label>
				<input class="woocommerce-Input woocommerce-Input--text input-text" type="text" name="telephone" id="telephone" value="<?php echo $d['telephone']; ?>" />
			</p> 

            <?php
                if (ICL_LANGUAGE_CODE=='lt') {
            ?>
			 
			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="license_number"><b><?php echo __( 'License number', 'moodle-synh' );  ?></b>&nbsp;<?php if (ICL_LANGUAGE_CODE=='lt') { ?><span class="required">*</span><?php } ?></label>
				<input class="woocommerce-Input woocommerce-Input--text input-text" type="text" name="license_number" id="license_number" value="<?php echo $d['license_number']; ?>" />
			</p>  
			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="working_company"><b><?php echo __( 'Working company', 'moodle-synh' );  ?></b>&nbsp;<?php if (ICL_LANGUAGE_CODE=='lt') { ?><span class="required">*</span><?php } ?></label>
				<input class="woocommerce-Input woocommerce-Input--text input-text" type="text" name="working_company" id="working_company" value="<?php echo $d['working_company']; ?>" />
			</p>   
			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="workplace"><b><?php echo __( 'Workplace/address', 'moodle-synh' );  ?></b>&nbsp;<?php if (ICL_LANGUAGE_CODE=='lt') { ?><span class="required">*</span><?php } ?></label>
				<input class="woocommerce-Input woocommerce-Input--text input-text" type="text" name="workplace" id="workplace" value="<?php echo $d['workplace']; ?>" />
			</p>
			 <?php
				}
	         ?>
			 
			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="working_place"><b><?php echo __( 'Working place', 'moodle-synh' );  ?></b>&nbsp;<?php if (ICL_LANGUAGE_CODE=='lt') { ?><span class="required">*</span><?php } ?></label>
				<input class="woocommerce-Input woocommerce-Input--text input-text" type="text" name="working_place" id="working_place" value="<?php echo $d['working_place']; ?>" />
			</p>  			 
			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="working_specialty"><b><?php echo __( 'Working specialty', 'moodle-synh' );  ?></b>&nbsp;<?php if (ICL_LANGUAGE_CODE=='lt') { ?><span class="required">*</span><?php } ?></label>
				<input class="woocommerce-Input woocommerce-Input--text input-text" type="text" name="working_specialty" id="working_specialty" value="<?php echo $d['working_specialty']; ?>" />
			</p> 
			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="working_address"><b><?php echo __( 'Working address', 'moodle-synh' );  ?></b>&nbsp;<?php if (ICL_LANGUAGE_CODE=='lt') { ?><span class="required">*</span><?php } ?></label>
				<input class="woocommerce-Input woocommerce-Input--text input-text" type="text" name="working_address" id="working_address" value="<?php echo $d['working_address']; ?>" />
			</p> 
			 
 <br>
  			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide" style="padding-bottom: 12px">
			<?php echo __( '<span class="required" style="font-weight: normal;">* Šie ir OBLIGĀTI norādāmie dati</span>', 'moodle-synh' );  ?>&nbsp;
            <BR> 
			</p>
            
 			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide" style="font-size: 12px;">
			<?php echo __( '<b>Personas datu apstrādes nolūks</b> – nodrošināt reģistrēšanos un  dalību Pasākumā, nodrošināt apliecinājuma izsniegšanu par dalību Pasākumā (ja konkrētā Pasākuma formāts paredz šāda apliecinājuma izsniegšanu), nosūtīt Jums informāciju par Pasākumu, uz kuru esat pieteicies, un informāciju par Olainfarm un tā grupas uzņēmumu produktiem un citiem Pasākumiem.  Lai personalizētu Jums sūtāmās informācijas saturu, var tikt ņemta vērā informācija par Jūsu specialitāti, profesionālās darbības vietu, amatu un iepriekšējo komunikāciju. ', 'moodle-synh' );  ?>&nbsp;</
			</p>
 			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide" style="font-size: 12px;">
			<?php echo __( '<b>Personas datu apstrādes pamats</b> - Olainfarm leģitīmās intereses īstenot iepriekš noteiktos nolūkus, tādā veidā sniedzot ieguldījumu sabiedrības veselības uzlabošanā un veicot komercdarbību.   ', 'moodle-synh' );  ?>&nbsp;</
			</p>      
 			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide" style="font-size: 12px;">
			<?php echo __( '<b>Informācija par pārzini</b> - Akciju sabiedrība “Olainfarm”, reģ.Nr.40003007246, kontaktinformācija personas datu aizsardzības jautājumos: Rūpnīcu iela 5, Olaine, Olaines novads, LV-2114, dataprotection@olainfarm.com, +371 28327856.', 'moodle-synh' );  ?>&nbsp;</
			</p> 

      <br><BR>
			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="get_info" style="cursor: pointer;"><input type="checkbox" id="get_info" name="get_info" value="1" <?php if ($d['get_info'] or !$d) echo "checked"; ?> > <?php echo __( 'Vēlos saņemt (e-pastā, pa tālruni) informāciju veselības aprūpes speciālistiem no Olainfarm par Olainfarm un tā grupas uzņēmumu produktiem un pasākumiem.', 'moodle-synh' );  ?>&nbsp;</label>
			</p>
                               
      <br>
			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="agree" style="cursor: pointer;"><input type="checkbox" id="agree" name="agree" onclick="terms_changed()" value="1" <?php if ($d['agree']) echo "checked"; ?> > <?php echo __( 'Apliecinu, ka informācija par personas datu apstrādi ir pieejama, ar to iepazinos un tā ir saprotama. Papildu informācija par personas datu apstrādi, tai skaitā par tiesībām iebilst, lūgt personas datu dzēšanu, pieejama <a href="https://lv.olainfarm.com/par-uznemumu/ilgtspeja-2/parvaldiba/marketinga-privatuma-pazinojums/?lang=lv" target="_blank">ŠEIT</a>.', 'moodle-synh' );  ?>&nbsp;</label>
			</p>
      

      

			<p class="form-row">
				<input type="hidden" id="woocommerce-login-nonce" name="woocommerce-login-nonce" value="74416baf00" /><input type="hidden" name="_wp_http_referer" value="/my-account/" />				<button type="submit" class="woocommerce-button button woocommerce-form-login__submit" disabled name="register" id="regFormSubmit" value="1"><?php echo __( 'Izveidot kontu', 'moodle-synh' );  ?></button>
			</p>
                                   
     </form>
<script>
function terms_changed(){
    termsCheckBox = document.getElementById("agree");
    //If the checkbox has been checked
    if(termsCheckBox.checked){
        //Set the disabled property to FALSE and enable the button.
        document.getElementById("regFormSubmit").disabled = false;
    } else{
        //Otherwise, disable the submit button.
        document.getElementById("regFormSubmit").disabled = true;
    }
}
terms_changed();
</script>				
     
            </div>
            <div class="col-lg-2"></div>
          </div>
      </div>
      
    </div></div>
</article>
</main>  
    
    <?php
    
    //$content = ob_get_contents();
   // ob_end_clean();
   //// return $content;
   echo $content;
    return ob_get_clean();
    
  }
  
  
	public function moowoodle_login_handler( $atts, $content = null ) {
		global $DC_Woodle, $curl, $moodle_login_error;
    
    ob_start();
    
    if ( is_admin()){
	return;
}
        	$access_url = woodle_get_settings( 'access_url', 'dc_woodle_general' );
          $access_token = woodle_get_settings( 'ws_token', 'dc_woodle_general' );
    ?>
<main class="col-xs-12 ">

					
<article id="post-12326" class="post-12326 page type-page status-publish hentry">

	<div class="entry-content">
	
		<div class="woocommerce"><div class="woocommerce-notices-wrapper"></div>

		
    
    <?php

   if($moodle_login_error)
   {
     ?>
        <div class="woocommerce-notices-wrapper">
        <ul class="woocommerce-error" role="alert">
        			<li>
        			<strong><?php echo __( $moodle_login_error, 'moodle-synh' ); ?></strong></li>
        	</ul>
        </div>
     <?php
   }
    
    if($_POST['login2'])
    {
    


          
          $restformat = ($restformat == 'json')?'&moodlewsrestformat=' . 
                         $restformat:'';
    
          $params=array();
          $serverurl = $access_url . '/login/token.php?username='.urlencode(trim($_POST['username'])).'&password='.urlencode(trim($_POST['password'])).'&service=moodle_mobile_app';
          $resp1 = $curl->get($serverurl . $restformat);
          $rest11 = json_decode($resp1, true);

    
          if ($rest11['error'])
          {
           ?>
<div class="woocommerce-notices-wrapper"><ul class="woocommerce-error" role="alert">
			<li>
			<strong><?php echo $rest11['error']; ?></li>
	</ul>
</div>
           <?php
          }
          else
          {
            
          
            if($rest11['token'])
            {
                $_SESSION['moodle_user']['token']=$rest11['token'];
                $_SESSION['moodle_user']['username']=trim($_POST['username']);
                
                //echo 'aaaaaa';
                
                $serverurl = $access_url . '/webservice/rest/server.php?wstoken='.$rest11['token'].'&wsfunction=core_user_get_users_by_field&field=username&values[0]='.trim($_POST['username']).'&moodlewsrestformat=json';
                //echo $serverurl;
                $resp1 = $curl->get($serverurl);
                $array = (json_decode($resp1, true));
                $_SESSION['moodle_user'] = $array[0];
               // ob_start();
                //Header("Location: ".$_SERVER['SCRIPT_URI'].'?auth=ok');
               // wp_redirect( 'http://moodle.integra.lv/moodle-kursi/?auth=ok' );
               // exit;
    $param_login = [
        'user' => [
            'username'  => trim($_POST['username']), 
        ]
    ];
        $serverurl = $access_url . '/webservice/rest/server.php' . '?wstoken=' . $access_token . '&wsfunction=auth_userkey_request_login_url&moodlewsrestformat=json';
        
        //echo $serverurl.'<BR><BR>';
        
        $resp     = $curl->post($serverurl, $param_login);
        $resp     = json_decode($resp);
        //print_r($resp);
        if ($resp && !empty($resp->loginurl)) {
            $loginurl = $resp->loginurl;        
        }  

         // echo $_SERVER['SCRIPT_URI'].'<BR><BR>';
         // print_r($loginurl);
         
          //ob_start();
          //$loginurl = $loginurl . '&wantsurl=' . urlencode($loginurl);
          //header("Location: ".$loginurl); 
          //echo 'aaa';                   
            }
          }
          

    }
    
    
    if (! $_SESSION['moodle_user'] )  {

    //print_r($_SERVER['SCRIPT_URI']);
    ?>
    
				<div class="container">
					<div class="row">
						<div class="col-lg-6">
    
    <center><h2><?php echo __( 'Autorizācija', 'moodle-synh' );  ?></h2></center>            
		<form class="woocommerce-form woocommerce-form-login login" method="post" action="">

			
			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="username"><?php echo __( 'Lietotājvārds', 'moodle-synh' );  ?>&nbsp;<span class="required">*</span></label>
				<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" autocomplete="username" value="" />			</p>
			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="password"><?php echo __( 'Parole', 'moodle-synh' );  ?>&nbsp;<span class="required">*</span></label>
				<input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" autocomplete="current-password" />
			</p>

			
			<p class="form-row">
				<button type="submit" class="woocommerce-button button woocommerce-form-login__submit" name="login" value="<?php echo __( 'Pieslēgties', 'moodle-synh' );  ?>"><?php echo __( 'Pieslēgties', 'moodle-synh' );  ?></button>
			</p>
            <?php
            $forgot_link =  get_permalink( apply_filters( 'wpml_object_id', 28, 'page', false, ICL_LANGUAGE_CODE ) );
            ?>			
			<p class="woocommerce-LostPassword lost_password">
				<a href="<?php echo $access_url;?>/login/forgot_password.php"><?php echo __( 'Aizmirsi paroli vai lietotājvārdu? Spied šeit!', 'moodle-synh' );  ?></a>
			</p>

			
		</form>
          
          </div>
          <div class="col-lg-6">
            <center><h2><?php echo __( 'Reģistrācija', 'moodle-synh' );  ?></h2></center>
            <?php
            $reg_link =  get_permalink( apply_filters( 'wpml_object_id', 11453, 'page', false, ICL_LANGUAGE_CODE ) );
            ?>			  
          	<form class="woocommerce-form woocommerce-form-login login" method="get" action="<?php echo $reg_link; ?>">
            <p>
			<?php echo __( 'Lai Jūs varētu piedalīties seminārā/ vebinārā vai citā akciju sabiedrības “Olainfarm” (turpmāk - Olainfarm) nodrošinātajā pasākumā (turpmāk - Pasākums), Jums ir jāizveido konts portālā veselības aprūpes speciālistiem “Open OlainFarm”.<br>
Izveidojot KONTU, Jums tiks nodrošināta iespēja:<br>
&nbsp;&bull;&nbsp;reģistrēties uz izvēlēto Pasākumu,<br>
&nbsp;&bull;&nbsp;saņemt kontā apliecinājumu par dalību Pasākumā,<br>
&nbsp;&bull;&nbsp;saņemt aktuālu informāciju veselības aprūpes speciālistiem.', 'moodle-synh' );  ?>	
			</p><br>

<button type="submit" class="woocommerce-button button woocommerce-form-login__submit" name="rb" value="1"><?php echo __( 'Reģistrēties', 'moodle-synh' );  ?></button>

            </form>
          </div>
          </div>
    </div>
    
    <?php } else {
    
    if (!$_SESSION['moodle_user']['customfields']['certificate'] && !$_SESSION['moodle_user']['customfields']['speciality']) {
        foreach($_SESSION['moodle_user']['customfields'] as $key2=>$value2)
        {
          
          if ($value2['shortname']=='certificate') $_SESSION['moodle_user']['customfields']['certificate'] = $value2['value'];
          else if ($value2['shortname']=='speciality') $_SESSION['moodle_user']['customfields']['speciality'] = $value2['value'];
        }
    }    
    
     ?>
    
	<div class="u-columns woocommerce-Addresses col2-set addresses">


	<div class="u-column1 col-1 woocommerce-Address">
		<header class="woocommerce-Address-title title">
    <table>
    <Tr>
      <td width="120px"><?php 
        if ($_SESSION['moodle_user']['profileimageurl'])
        {
          echo '<img src="'.$_SESSION['moodle_user']['profileimageurl'].'" class="roundimage">';
        }
      ?></td>
    <Td>
			<h2><?php if ($_SESSION['moodle_user']['firstname'] ) { echo  $_SESSION['moodle_user']['firstname']." ".$_SESSION['moodle_user']['lastname']; } else
      {echo $_SESSION['moodle_user']['fullname']; }?> </h2>
    </td>
    <tr>
    </table>  
    
		</header>
		<p><b><?php echo __( 'Lietotājs', 'moodle-synh' );  ?>:</b> <?php echo  $_SESSION['moodle_user']['username']."<br>
      <b>".__( 'E-pasts', 'moodle-synh' ).':</b> '.$_SESSION['moodle_user']['email']."<br>"; ?>
      
      <?php
      if ($_SESSION['moodle_user']['city'])
      {
        echo "<b>".__( 'Pilsēta', 'moodle-synh' ).":</b> ".$_SESSION['moodle_user']['city']."<br>" ;
      }        
      if ($_SESSION['moodle_user']['customfields']['speciality'])
      {
      //  echo "<b>".__( 'Profesija', 'moodle-synh' ).":</b> ".$_SESSION['moodle_user']['customfields']['speciality']."<br>";
      }
      if ($_SESSION['moodle_user']['customfields']['certificate'])
      {
        echo "<b>".__( 'Sertifikāta Nr.', 'moodle-synh' ).":</b> ".$_SESSION['moodle_user']['customfields']['certificate']."<br>" ;
      }
    
      ?>
      
    </p>
    
					<div class="row">
						<div class="col-lg-6">
 <a href="<?php echo $access_url;?>/user/profile.php" target="_blank"><div class="advantages-course__box advantages-course__tag bg-blue">
                        <div class="advantages-course-icon"><img src="<?php echo get_template_directory_uri(); ?>/img/courses/icon3.png"></div>
                        <p><?php echo __( 'Labot profilu', 'moodle-synh' ); ?></p>
                    </div></a>   
            </div>
            <div class="col-lg-6">
 <a href="<?php echo $access_url;?>/mod/customcert/my_certificates.php?userid=<?php echo $_SESSION['moodle_user']['id']; ?>" target="_blank"><div class="advantages-course__box advantages-course__tag bg-blue">
                        <div class="advantages-course-icon"><img src="<?php echo get_template_directory_uri(); ?>/img/courses/icon2.png"></div>
                        <p><?php echo __( 'Mani sertifikāti', 'moodle-synh' ); ?></p>
                    </div></a>  
            </div>
          </div>        
                    
    
    <p><a href="<?php echo get_permalink();?>?action=moodle_logout" class="btn-read-more"><?php echo __( 'Iziet', 'moodle-synh' );  ?></a></p>
	</div>



	</div>
    
    <?php } ?>

</div>

	</div><!-- .entry-content -->
</article><!-- #post-## -->

				</main> 
    <?php
     //$content = ob_get_contents();

    //ob_end_clean();

    echo $content;
    
      return ob_get_clean();
         
    }
  
	public function moowoodle_register_handler( $atts, $content = null ) {
		global $DC_Woodle;
       if ( is_admin()){
	return;
}

    ?>
<div class="woocommerce">
    
<div class="woocommerce-MyAccount-content">
	<div class="woocommerce-notices-wrapper"></div>
<form class="woocommerce-EditAccountForm edit-account" action="" method="post"  >

	
	<p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
		<label for="account_first_name">First name&nbsp;<span class="required">*</span></label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_first_name" id="account_first_name" autocomplete="given-name" value="" />
	</p>
	<p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
		<label for="account_last_name">Last name&nbsp;<span class="required">*</span></label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_last_name" id="account_last_name" autocomplete="family-name" value="" />
	</p>
	<div class="clear"></div>

	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="account_display_name">Display name&nbsp;<span class="required">*</span></label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_display_name" id="account_display_name" value="admin" /> <span><em>This will be how your name will be displayed in the account section and in reviews</em></span>
	</p>
	<div class="clear"></div>

	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="account_email">Email address&nbsp;<span class="required">*</span></label>
		<input type="email" class="woocommerce-Input woocommerce-Input--email input-text" name="account_email" id="account_email" autocomplete="email" value="marina@integra.lv" />
	</p>

	<fieldset>
		<legend>Password change</legend>

		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="password_current">Current password (leave blank to leave unchanged)</label>
			<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_current" id="password_current" autocomplete="off" />
		</p>
		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="password_1">New password (leave blank to leave unchanged)</label>
			<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_1" id="password_1" autocomplete="off" />
		</p>
		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="password_2">Confirm new password</label>
			<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_2" id="password_2" autocomplete="off" />
		</p>
	</fieldset>
	<div class="clear"></div>

	
	<p>
		<input type="hidden" id="save-account-details-nonce" name="save-account-details-nonce" value="7688242cb4" /><input type="hidden" name="_wp_http_referer" value="/my-account/edit-account/" />		<button type="submit" class="woocommerce-Button button" name="save_account_details" value="Save changes">Save changes</button>
		<input type="hidden" name="action" value="save_account_details" />
	</p>

	</form>

</div>
</div>

</div>
    <?php
    		  return ;
  }  
	public function moowoodle_handler( $atts, $content = null ) {
		global $DC_Woodle;
    if ( is_admin()){
	return;
}
	// clone attribs over any default values, builds variables out of them so we can use them below
	// $class => css class to put on link we build
	// $cohort => text id of the moodle cohort in which to enrol this user
	// $group => text id of the moodle group in which to enrol this user
	// $course => text id of the course, if you just want to enrol a user directly to a course
	// $authtext => string containing text content to display when not logged on (defaults to content between tags when empty / missing)
	// $activity => index of the first activity to open, if autoopen is enabled in moodle
  
  if (!$_SESSION['moodle_user'] )  
  {

     return do_shortcode( "[moowoodle_login]" );
      
     // echo __( 'Sadaļa ir pieejama tikai autorizētiem lietotājiem.<br>
     ///11 Lūdzu <a href="/autorizacija/">ielogoties</a> vai <a href="/registracija/">izveidot jaunu kontu</a>.', 'moodle-synh' );
      

      return;
  }

		extract(shortcode_atts(array(
			"cohort" => '',
			"group" => '',
			"course" => '',
			"class" => 'moowoodle',
			"target" => '_self',
			"authtext" => '',
			"activity" => 0
			), $atts));

    	$access_url = woodle_get_settings( 'access_url', 'dc_woodle_general' );
      $access_token = woodle_get_settings( 'ws_token', 'dc_woodle_general' );

      
    if (1==1)
    {
      
  // echo $access_url."/webservice/rest/server.php?wstoken=".$access_token."&wsfunction=core_course_get_courses&moodlewsrestformat=json";

$json_cats = file_get_contents($access_url."/webservice/rest/server.php?wstoken=".$access_token."&wsfunction=core_course_get_courses&moodlewsrestformat=json");
$array_cats = (json_decode($json_cats, true));
$CATEGORIES=array();
//echo '<pre>';

foreach($array_cats as $key=>$value)
{
  if (!in_array($value['categoryid'],$CATEGORIES))
  $CATEGORIES[$value['categoryid']] = $value['displayname'];
}
   
//print_r($CATEGORIES);
$json = file_get_contents($access_url."/webservice/rest/server.php?wstoken=".$access_token."&wsfunction=core_course_get_courses&moodlewsrestformat=json") ;


//echo $access_url."/webservice/rest/server.php?wstoken=".$access_token."&wsfunction=core_course_get_courses&moodlewsrestformat=json";          

//echo '<pre>';
//print_r(json_decode($json, true));

$array = (json_decode($json, true));


$CARRAY=array();


//echo date('Y-m-d G:i:s');


$i=0;
foreach($array as $key=>$value)
{
    if ($value['visible']==1 && $key<>0)
    {
    //echo $value['id'] . "=>" . $value['fullname'] . " ".$value['displayname']." ".$value['categorysortorder']." ".($value['startdate']?date("d.m.Y",$value['startdate']):'')." ".($value['enddate']?date("d.m.Y",$value['enddate']):'')."<br>";
    
    $CARRAY[$i]['name'] = $value['fullname'];
    $CARRAY[$i]['categoryid'] = $value['categoryid'];
    $CARRAY[$i]['categorysortorder'] = $value['categorysortorder'];
    $CARRAY[$i]['startdate'] = ($value['startdate']?date("d.m.Y",$value['startdate']):'');
    //$CARRAY[$i]['startdate'] = 
    $CARRAY[$i]['enddate'] = ($value['enddate']?date("d.m.Y",$value['enddate']):'');
    $CARRAY[$i]['id'] = $value['id']; 
    
  // echo '<pre>';
    //print_r($value['customfields']);
    ////exit();
    
    foreach($value['customfields'] as $key2=>$value2)
    {
      

      if ($value2['shortname']=='points') $CARRAY[$i]['points'] = $value2['value'];
      else if ($value2['shortname']=='author') $CARRAY[$i]['author'] = $value2['value'];
      else if ($value2['shortname']=='points_author') $CARRAY[$i]['points_author'] = $value2['value'] ;
    }
  
    
    $i++;
    }
}

$this->array_sort_by_column($CARRAY, 'categorysortorder');
/*print_r($CARRAY);
      */
      ?>
      
<h2><?php echo __( 'Piejamie kursi', 'moodle-synh' );  ?></h2>
<div class="wales_table">

<table width="100%" border="0">

<tr>
  <td><b><?php echo __( 'Kursa nosaukums', 'moodle-synh' );  ?></b></td>
  <td><b><?php echo __( 'Autors', 'moodle-synh' );  ?></b></td>
  <td><b><?php echo __( 'Datumi (no-līdz)', 'moodle-synh' );  ?></b></td>
  <td><b><?php echo __( 'TIP', 'moodle-synh' );  ?></b></td>
  <td><b><?php echo __( 'Punktu sniedzējs', 'moodle-synh' );  ?></b></td>
  <td></td>
</tr>

  <tbody>
  
  
  
  <?php
  
  foreach($CARRAY as $key=>$value)
{
  ?>
  <tr>

    <td>
      <div><b><?php echo $value['name']; ?></b><br>Kategorija: <?php echo $CATEGORIES[$value['categoryid']]; ?></div>
    </td>
    
    <td><?php echo $value['author']; ?></td>
    <td><?php echo $value['startdate'].' '.($value['enddate']?' - '.$value['enddate']:''); ?></td>
    <td><?php echo $value['points']; ?></td>
    <td><?php echo $value['points_author']; ?></td>

    <td align="center"><a href="<?php echo $access_url.'/course/view.php?id='.$value['id']; ?>" target="_blank" class="order_btn">Pieteikties</a></td>
  </tr>
 <?php } ?>
   
  
  </tbody>
  </table>
  
     </div>
      <?php
    }
      
		  return ;
	}

	public function moowoodle_add_plugin($plugin_array) {
	   $plugin_array['moowoodle'] = plugin_dir_url(__FILE__).'moowoodle.js';
	   return $plugin_array;
	}

}